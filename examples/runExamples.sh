#!/bin/bash

# Build main project
./gradlew clean jar

cd examples

# Run Java example
./gradlew runJavaExample

# Run Kotlin example
./gradlew runKotlinExample
