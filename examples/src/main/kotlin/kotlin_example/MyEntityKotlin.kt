package kotlin_example

import org.hsmith.hibernate_decorator.data.entity.impl.EntityWithLongIdBase
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity
@Table(name = "kotlin_MyEntity")
class MyEntityKotlin : EntityWithLongIdBase() {

    @Column(name = "long_value")
    var longValue: Long = 0
}