package kotlin_example

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.slf4j.LoggerFactory

class App {
    private val logger = LoggerFactory.getLogger(App::class.java)

    fun run() {
        logger.info("Running kotlin example project")
        val dbApi = HibernateDecoratorApi.newDbApiBuilder().build()
        dbApi.use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MyEntityKotlin::class.java)

                for (i in 0..8) {
                    val myEntity = MyEntityKotlin()
                    myEntity.longValue = i * 10L
                    repo.save(myEntity)
                }
            }

            api.newSession().use { session ->
                val repo = session.newRepository(MyEntityKotlin::class.java)
                val entities = repo.all()

                logger.info("Entities in database:")
                for (entity in entities) {
                    logger.info("${entity.id}: ${entity.longValue}")
                }
            }
        }
    }
}

fun main() {
    App().run()
}
