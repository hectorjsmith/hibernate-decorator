package java_example;

import org.hsmith.hibernate_decorator.data.entity.impl.EntityWithLongIdBase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "java_MyEntity")
public class MyEntityJava extends EntityWithLongIdBase {
    
    private Long longValue;
    
    @Column(name = "long_value")
    public Long getLongValue() {
        return longValue;
    }

    public void setLongValue(final Long longValue) {
        this.longValue = longValue;
    }

    @Override
    public void setId(@NotNull final Long aLong) {
        super.setId(aLong.longValue());
    }
}
