package java_example;

import org.hsmith.hibernate_decorator.api.DbApi;
import org.hsmith.hibernate_decorator.api.DbSession;
import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi;
import org.hsmith.hibernate_decorator.data.repository.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
    private final Logger logger = LoggerFactory.getLogger(App.class);
    
    public static void main(String[] args) {
        new App().run();
    }

    private void run() {
        logger.info("Running Java example project");
        
        DbApi dbApi = HibernateDecoratorApi.INSTANCE.newDbApiBuilder().build();
        try (DbSession session = dbApi.newSession()) {
            Repository<MyEntityJava> repo = session.newRepository(MyEntityJava.class);

            for (int i = 0; i < 9; i++) {
                MyEntityJava myEntity = new MyEntityJava();
                myEntity.setLongValue(i * 10L);
                repo.save(myEntity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (DbSession session = dbApi.newSession()) {
            Repository<MyEntityJava> repo = session.newRepository(MyEntityJava.class);
            Iterable<MyEntityJava> entities = repo.all();

            logger.info("Entities in database:");
            for (MyEntityJava entity : entities) {
                logger.info(entity.getId() + " : " + entity.getLongValue());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
