# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Support for limiting where queries using both the `Query` and `JpqlQuery` objects (#31) 

## [0.3.0] - 2020-11-19

### Added
- New `Query` object to handle complex database queries (#34)
- New `JpqlQuery` support to facilitate running raw JPQL queries (#37)
- Build `DbSession` entity from JPA entity manager (#36)

### Changed
- [BREAKING] Remove methods on the `Repository` interface that take `WhereClause` instances - the new `Query` object should be used (#34)
- [BREAKING] Move basic queries to new package - was `org.hsmith.hibernate_decorator.query.impl`, now is `org.hsmith.hibernate_decorator.query.jpa` (#34)

## [0.2.0] - 2020-10-28

### Changed
- [BREAKING] Renamed methods on the `Repository` and `EntityWithIdRepository` interfaces to have a more succinct name (#25)
- Replace random session IDs with UUIDs (#32)

### Added
- Overloads for methods on the `Repository` interface to simplify the creation of `field = value` type queries (#25)
- Methods on the `DbApiBuilder` to override common properties (#23)
- Support for creating a `DbApi` from an existing `EntityManagerFactory` (#30)
- Gradle configuration to publish package to Gitlab (#26)
- Kdoc for all public interfaces and enums (#28)

## [0.1.0] - 2020-07-24

- Initial release
