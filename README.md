# Hibernate Decorator

Kotlin library to wrap around hibernate and make database access simpler.

[![pipeline status](https://gitlab.com/hectorjsmith/hibernate-decorator/badges/main/pipeline.svg)](https://gitlab.com/hectorjsmith/hibernate-decorator/-/commits/main)

## Quick start

*Kotlin*

```kotlin
val dbApi = HibernateDecoratorApi.newDbApiBuilder().build()
    dbApi.use { api ->
        api.newSession().use { session ->
            // Get repository
            val repository = session.newRepository(MyEntity::class.java)

            // Find all matching entities
            val matchingEntities: Iterable<MyEntity> = repository
                        .newQuery()
                        .where("myField", 25)
                        .all()

            // Save new entity
            val newEntity = MyEntity()
            newEntity.myField = 25
            repository.save(newEntity)

            // Delete entities based on condition
            val whereFieldLessThan10: WhereClause = HibernateDecoratorApi
                    .newWhereClauseFactory()
                    .newWhereClause("myField", BinaryExpressionOperator.LESS_THAN, 10)
            val deletedCount: Long = repository
                    .newQuery()
                    .where(whereFieldLessThan10)
                    .delete()
        }
}
```

## Table of Contents

1. Add dependency
1. Database connection
    1. Persistence.xml
    1. HibernateDecoratorApi
    1. Use existing EntityManagerFactory
1. Create new session
1. Define an entity
1. Save an entity
1. Query the database
    1. Where clauses
    1. Raw JPQL queries
1. Delete an entity
1. Examples

## 1. Add dependency

Find the latest compiled packages for this project on the [Gitlab Package Registry](https://gitlab.com/hectorjsmith/hibernate-decorator/-/packages).

To include this library in your project you need to add the Gitlab Package Registry as a maven repository to your `build.gradle` file.

```
repositories {
    maven {
        url "https://gitlab.com/api/v4/projects/19284563/packages/maven"
    }
}
```

You can then add a dependency on the library itself.

```
dependencies {
    implementation 'org.hsmith:hibernate-decorator:0.3.0'
}
```

Note that you will also need to add dependencies for `hibernate` and the database type you are planning to use.

## 2. Connect to database

### 2.1. Persistence.xml

To set up a database connection, set the appropriate parameters in the `persistence.xml` file.
Hibernate will process this file on start-up and use it to configure the database connection.
This file must be in the JPA format.

The file should be saved at `resources/META-INF/persistence.xml`.

### 2.2. HibernateDecoratorApi

The `HibernateDecoratorApi` class is the main access point of this library and should be used to create a database API instance.

```kotlin
val api: DbApi = HibernateDecoratorApi.newDbApiBuilder().build()
```

You can also set additional parameters when building the API instance:
- `withPersistenceUnitName` - This allows selecting a specific persistence unit from the `persistence.xml` file. It defaults to `default`.
- `withPersistencePropertyOverride` - This allows setting up overrides for parameters defined in the `persistence.xml` file. Any key-value pairs provided here will override values from the `persistence.xml` file.

### 2.3. Use existing EntityManagerFactory

Instead of building a new EntityManagerFactory object when creating the `DbApi` instance, it is possible to provide an existing one.

```kotlin
val builder: DbApiBuilder = HibernateDecoratorApi.newDbApiBuilder()
builder.withEntityManagerFactory(existingEntityManagerFactory)
val api = builder.build()
```

This allows this library to be used in situations where there is complex logic around creating the EntityManagerFactory that the library cannot handle.

When you use the `withEntityManagerFactory` option all other options are ignored.
This is because all the other options define how to create a new EntityManagerFactory instance - since you are providing an existing instance, no new instances will be created.

## 3. Create new session

All database access through this library must be done using a session. Use the `DbApi` instance to get a new session object.

```kotlin
val session: DbSession = api.newSession()
```

Once you are done with that session, make sure to close it.

```kotlin
session.close()
```

This will automatically try to commit the transaction (or roll it back if the transaction has been flagged for roll-back).

Since the `DbSession` object extends `AutoCloseable` you can also use it as a block and have it closed automatically.

```kotlin
api.newSession().use { session ->
    // ...
}
```

## 4. Define an entity

Entity definition does not change when using this library. The same JPA annotations should be used.

However, for best results, consider extending the following interfaces in your entities where applicable:

- `Entity`
- `EntityWithId`
- `EntityWithCreateTime`
- `EntityWithUpdateTime`

Take a look at the implementation classes for these interfaces for examples on how to implement them.
You can also extend one of the base implementations to avoid writing it yourself.
If your entity implements multiple interfaces you will have to only extend one base class and re-implement the rest because it is not possible to do multiple-inheritance.

Doing this will allow you to use the pre-defined queries for that entity type (e.g. find entity by ID).

## 5. Save an entity

To save an entity to the database, first get a repository off the session object:

```kotlin
val repository: EntityRepository<MyEntity> = session.newRepository(MyEntity::class.java)
```

You can then use the provided methods to save your entity:

```kotlin
val entity: MyEntity = MyEntity()
// ...
repository.save(entity)
```

Note that repositories do not need to be closed and apply to a single session.

Note that where clauses are not specific to any session or entity and can be re-used.

## 6. Query the database

To run a database query you will need an entity repository instance. Once a repository has been created, it is possible to use the `all` and `where` method to query the database for entities.

To return all entities from the database, use the `all` method:

```kotlin
// Create repository
val repository: EntityRepository<MyEntity> = session.newRepository(MyEntity::class.java)

// Find all entities
val allEntities: Iterable<MyEntity> = repository.all()
```

It is possible to only retrieve entities that match a certain condition using the `where` method.
The following example finds all instances of `MyEntity` where the value of the `name` property equals "John":

```kotlin
// Create repository
val repository: EntityRepository<MyEntity> = session.newRepository(MyEntity::class.java)

// Find all matching entities
val matchingEntities: Iterable<MyEntity> = repository.where("name", "John")
```

### 6.1. Where clauses

The `WhereClause` interface allows defining more complex conditions for a query. For example, find all entities where `age` is greater than 25.

Instances of where clauses can be created using the `HibernateDecoratorApi` class. Clauses can be created either using a simple condition, or by combining multiple clauses together.

The following example creates a where clause that can be used to find all instances where the `name` property equals "John" **and** the `age` property is greater than 25.

```kotlin
val nameMatches: WhereClause = HibernateDecoratorApi.newWhereClauseFactory().newWhereClause("name", BinaryExpressionOperator.EQUAL, "John")
val ageMatches: WhereClause = HibernateDecoratorApi.newWhereClauseFactory().newWhereClause("age", BinaryExpressionOperator.GREATER_THAN, 25)

val nameAndAgeMatches = HibernateDecoratorApi.newWhereClauseFactory().newWhereClause(nameMatches, ClauseOperator.AND, ageMatches)
```

Once you have a where clause, you can use the same `where` method to find all entities that match that condition:

```kotlin
// Find all matching entities
val matchingEntities: Iterable<MyEntity> = repository.where(nameAndAgeMatches)
```

### 6.2. Raw JPQL queries

To support more complex use-cases it is possible to execute raw JPQL queries.

Build a new `JpqlQuery` from a `DbSession` instance by providing the raw JPQL query string, and a map of named parameters to value for substitution.

The following example runs a select query based on the provided JPQL query string:

```kotlin
api.newSession().use { session ->
    val query = session.newJpqlQuery(
        Person::class.java,
        "SELECT p FROM Person p WHERE p.age = :age AND p.name = :name",
        mapOf(Pair("age", 25), Pair("name", "John"))
    )
    val results = query.resultList()
}
```

## 7. Delete an entity

Just like saving entities, it is easy to delete entities using an entity repository instance. It is possible to either delete a single entity or multiple entities at once.

The following example deletes all entities where the `name` property equals "John":

```kotlin
// Create repository
val repository: EntityRepository<MyEntity> = session.newRepository(MyEntity::class.java)

// Create clause
val nameMatches: WhereClause = HibernateDecoratorApi.newWhereClauseFactory().newWhereClause("name", BinaryExpressionOperator.EQUAL, "John")

// Delete all matching entities
val numberOfEntitiesDeleted = repository.delete(nameMatches)
```

## 8. Examples

The project includes two example projects to see the library in action.
One project is Java-based while the other is Kotlin-based.

Both example projects have a dependency on the build output of the main library.
This ensures they always use the most up-to-date version of the library.
Both example projects are also ran during the gitlab CI build pipeline to ensure they always compile and run.
