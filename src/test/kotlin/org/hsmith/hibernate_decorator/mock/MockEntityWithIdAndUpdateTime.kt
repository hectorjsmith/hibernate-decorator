package org.hsmith.hibernate_decorator.mock

import javax.persistence.*
import org.hsmith.hibernate_decorator.data.entity.impl.EntityWithUpdateTimeBase

@Entity
open class MockEntityWithIdAndUpdateTime : EntityWithUpdateTimeBase() {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    open var id: Long = 0
}
