package org.hsmith.hibernate_decorator.mock

import javax.persistence.Column
import javax.persistence.Entity

@Entity
open class MockEntityWithIdAndLongField : MockBaseEntity() {
    @Column(name = "custom_field_long")
    open var customFieldLong: Long = 0
}
