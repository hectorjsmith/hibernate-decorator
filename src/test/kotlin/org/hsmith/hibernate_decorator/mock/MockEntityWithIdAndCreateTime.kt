package org.hsmith.hibernate_decorator.mock

import javax.persistence.*
import org.hsmith.hibernate_decorator.data.entity.impl.EntityWithCreateTimeBase

@Entity
open class MockEntityWithIdAndCreateTime : EntityWithCreateTimeBase() {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    open var id: Long = 0
}
