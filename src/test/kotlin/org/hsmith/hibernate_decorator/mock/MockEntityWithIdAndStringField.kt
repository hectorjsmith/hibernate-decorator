package org.hsmith.hibernate_decorator.mock

import javax.persistence.Column
import javax.persistence.Entity

@Entity
open class MockEntityWithIdAndStringField : MockBaseEntity() {
    @Column(name = "custom_field_str")
    open var customFieldStr: String = ""
}
