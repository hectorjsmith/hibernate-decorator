package org.hsmith.hibernate_decorator.mock

import javax.persistence.Entity
import org.hsmith.hibernate_decorator.data.entity.impl.EntityWithLongIdBase

@Entity
open class MockEntityWithLongId : EntityWithLongIdBase()
