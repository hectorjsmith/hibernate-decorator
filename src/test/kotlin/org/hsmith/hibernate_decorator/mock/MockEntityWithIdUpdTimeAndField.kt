package org.hsmith.hibernate_decorator.mock

import javax.persistence.Column
import javax.persistence.Entity

@Entity
open class MockEntityWithIdUpdTimeAndField : MockEntityWithIdAndUpdateTime() {
    @Column(name = "long_field")
    open var longField: Long = 0L
}
