package org.hsmith.hibernate_decorator.mock

import javax.persistence.Entity
import javax.persistence.Inheritance
import javax.persistence.InheritanceType
import org.hsmith.hibernate_decorator.data.entity.impl.EntityWithLongIdBase

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
open class MockBaseEntity : EntityWithLongIdBase()
