package org.hsmith.hibernate_decorator.api.impl

import javax.persistence.Column
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class DbApiImplTest {
    @Test
    fun `GIVEN empty database WHEN new entity saved THEN entity returned from all`() {
        // Assemble
        val sampleValue = "mock-sample-value"
        val mockEntity = MockDbEntity()
        mockEntity.sampleField = sampleValue

        // Act
        var allEntities: Iterable<MockDbEntity> = listOf()
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockDbEntity::class.java)
                repo.save(mockEntity)
                allEntities = repo.all()
            }
        }

        // Assert
        assertEquals(1, allEntities.count(), "One entity should be saved in database")
        assertEquals(sampleValue, allEntities.first().sampleField, "Data retrieved from database should match data saved")
    }

    @Test
    fun `GIVEN empty database WHEN new entity saved THEN countAll returns 1`() {
        // Assemble
        val mockEntity = MockDbEntity()

        // Act
        var entityCount = 0L
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockDbEntity::class.java)
                repo.save(mockEntity)
                entityCount = repo.count()
            }
        }

        // Assert
        assertEquals(1, entityCount, "One entity should be saved in database")
    }
}

@javax.persistence.Entity
class MockDbEntity() {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0

    @Column(name = "sample_field")
    var sampleField: String = ""
}
