package org.hsmith.hibernate_decorator.api.builder

import javax.persistence.PersistenceException
import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class DbApiBuilderTest {

    @Test
    fun `GIVEN db api builder WHEN invalid persistence unit name given THEN exception thrown`() {
        // Assemble
        val persistenceUnitName = "invalid-name"
        val builder = HibernateDecoratorApi.newDbApiBuilder()

        // Act
        val updatedBuilder = builder.withPersistenceUnitName(persistenceUnitName)

        // Assert
        assertThrows(PersistenceException::class.java, { updatedBuilder.build() },
                "Expected an exception when trying to build a database API with an invalid persistence unit name")
    }

    @Test
    fun `GIVEN db api builder WHEN connection URL overridden THEN change stored in property overrides`() {
        // Assemble
        val propertyToOverride = "hibernate.connection.url"
        val propertyNewValue = "[test] connection url"
        val builder = HibernateDecoratorApi.newDbApiBuilder()

        // Act
        val updatedBuilder = builder.withPersistencePropertyOverride(propertyToOverride, propertyNewValue)

        // Assert
        assertTrue(updatedBuilder.propertyOverrides.containsKey(propertyToOverride),
                "Property ID should exist in overrides map")

        val newValue = updatedBuilder.propertyOverrides[propertyToOverride]
        assertEquals(propertyNewValue, newValue,
                "Stored property value should match override value provided")
    }

    @Test
    fun `GIVEN db api builder WHEN connection URL overridden with invalid value THEN exception thrown`() {
        // Assemble
        val propertyToOverride = "hibernate.connection.url"
        val propertyNewValue = "[test] invalid connection url"
        val builder = HibernateDecoratorApi.newDbApiBuilder()

        // Act
        val updatedBuilder = builder.withPersistencePropertyOverride(propertyToOverride, propertyNewValue)

        // Assert
        assertThrows(PersistenceException::class.java, { updatedBuilder.build() },
                "Expected an exception when trying to build a database API with invalid properties")
    }

    @Test
    fun `GIVEN db api builder WHEN property overridden with enum key THEN change stored in property overrides`() {
        // Assemble
        val propertyToOverride = PersistencePropertyKey.DIALECT
        val propertyNewValue = "[test] dialect"
        val builder = HibernateDecoratorApi.newDbApiBuilder()

        // Act
        val updatedBuilder = builder.withPersistencePropertyOverride(propertyToOverride, propertyNewValue)

        // Assert
        assertTrue(updatedBuilder.propertyOverrides.containsKey(propertyToOverride.keyString),
                "Property ID should exist in overrides map")

        val newValue = updatedBuilder.propertyOverrides[propertyToOverride.keyString]
        assertEquals(propertyNewValue, newValue,
                "Stored property value should match override value provided")
    }

    @Test
    fun `GIVEN db api builder WHEN property overridden and API built THEN property set in generated API`() {
        // Assemble
        val propertyToOverride = "hibernate.c3p0.max_size"
        val propertyNewValue = "1000"
        val builder = HibernateDecoratorApi.newDbApiBuilder()

        // Act
        val updatedBuilder = builder.withPersistencePropertyOverride(propertyToOverride, propertyNewValue)
        val api = updatedBuilder.build()
        val properties = api.rawEntityManagerFactory.properties

        // Assert
        assertTrue(properties.containsKey(propertyToOverride),
                "Property not found in generated API for c3p0 min size")

        val storedProperty = properties[propertyToOverride]
        assertEquals(propertyNewValue, storedProperty,
                "Stored property value should match override value provided")
    }

    @Test
    fun `GIVEN db api builder WHEN entity manager factory provided THEN all other options are ignored`() {
        // Assemble
        val invalidPersistenceUnitName = "invalid-name"
        val rawEntityManagerFactory = HibernateDecoratorApi.newDbApiBuilder().build().rawEntityManagerFactory
        val builder = HibernateDecoratorApi.newDbApiBuilder()

        // Act
        val updatedBuilder = builder
                .withEntityManagerFactory(rawEntityManagerFactory)
                .withPersistenceUnitName(invalidPersistenceUnitName)

        // Assert
        assertDoesNotThrow({ updatedBuilder.build() },
                "Did not expected an exception when trying to build a database API with an invalid persistence unit name." +
                        " The provided entity manager factory should be used instead.")
    }
}
