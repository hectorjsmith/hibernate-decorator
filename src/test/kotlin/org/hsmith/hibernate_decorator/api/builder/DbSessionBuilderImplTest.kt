package org.hsmith.hibernate_decorator.api.builder

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithLongId
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class DbSessionBuilderImplTest {
    @Test
    fun `GIVEN entity manager factory WHEN new db session build THEN db session is open`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityManagerFactory = api.rawEntityManagerFactory

            // Act
            val dbSession = HibernateDecoratorApi
                    .newDbSessionBuilder(entityManagerFactory)
                    .build()

            // Assert
            dbSession.use {
                assertTrue(it.entityManagerDecorator.isSessionOpen, "DbSession should be open after construction")
            }
        }
    }

    @Test
    fun `GIVEN entity manager WHEN new db session build THEN db session is open`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityManager = api.rawEntityManagerFactory.createEntityManager()

            // Act
            val dbSession = HibernateDecoratorApi
                    .newDbSessionBuilder(entityManager)
                    .build()

            // Assert
            dbSession.use {
                assertTrue(it.entityManagerDecorator.isSessionOpen, "DbSession should be open after construction")
            }
        }
    }

    @Test
    fun `GIVEN entity manager factory WHEN new db session build THEN db session works as expected`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityManager = api.rawEntityManagerFactory

            // Act
            val dbSession = HibernateDecoratorApi
                    .newDbSessionBuilder(entityManager)
                    .build()

            // Assert
            dbSession.use {
                val repo = it.newRepository(MockEntityWithLongId::class.java)
                assertEquals(repo.count(), 0, "No entities should exist in database")
                repo.save(MockEntityWithLongId())
                assertEquals(repo.count(), 1, "New entity should exist in database")
            }
        }
    }

    @Test
    fun `GIVEN entity manager factory WHEN new db session build with custom session id THEN db session has correct session id`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val mockSessionId = "mock session id"
            val entityManager = api.rawEntityManagerFactory

            // Act
            val dbSession = HibernateDecoratorApi
                    .newDbSessionBuilder(entityManager)
                    .withSessionId(mockSessionId)
                    .build()

            // Assert
            dbSession.use {
                assertEquals(mockSessionId, it.sessionId, "DbSession ID should match provided session ID")
            }
        }
    }

    @Test
    fun `GIVEN entity manager factory WHEN new db session build without session id THEN db session is not blank`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityManager = api.rawEntityManagerFactory

            // Act
            val dbSession = HibernateDecoratorApi
                    .newDbSessionBuilder(entityManager)
                    .build()

            // Assert
            dbSession.use {
                assertFalse(it.sessionId.isBlank(), "DbSession ID should not be blank")
            }
        }
    }
}
