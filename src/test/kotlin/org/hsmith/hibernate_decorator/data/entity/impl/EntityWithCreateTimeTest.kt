package org.hsmith.hibernate_decorator.data.entity.impl

import java.time.LocalDateTime
import java.time.ZoneOffset
import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndCreateTime
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdCreTimeAndField
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class EntityWithCreateTimeTest {

    @Test
    fun `GIVEN entity with creation time WHEN saved to database THEN entity creation time is correctly set`() {
        // Assemble
        var savedEntityId: Long = -1
        val entityToSave = MockEntityWithIdAndCreateTime()
        var saveTime: LocalDateTime? = null
        assertNull(entityToSave.creationTime, "GIVEN: Creation date should be null before test")

        // Act
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndCreateTime::class.java)
                saveTime = LocalDateTime.now()
                savedEntityId = repo.save(entityToSave).id
            }
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndCreateTime::class.java)
                val savedEntity = repo.all().first { it.id == savedEntityId }

                // Assert
                assertNotNull(savedEntity.creationTime, "Creation time should not be null after save")
                val timeDiff = Math.abs(saveTime!!.toInstant(ZoneOffset.UTC)!!.toEpochMilli() - savedEntity.creationTime!!.toInstant(ZoneOffset.UTC)!!.toEpochMilli())
                assertTrue(timeDiff < 100, "Creation time should be within a second of actual save time")
            }
        }
    }

    @Test
    fun `GIVEN entity with creation time WHEN change saved to database THEN entity creation time is not changed`() {
        // Assemble
        val entityFieldNewValue = 200L
        val entityToSave = MockEntityWithIdCreTimeAndField()
        entityToSave.longField = 100
        var savedEntity: MockEntityWithIdCreTimeAndField? = null
        var originalUpdateTime: LocalDateTime? = null
        assertNull(entityToSave.creationTime, "GIVEN: Creation date should be null before test")

        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdCreTimeAndField::class.java)
                savedEntity = repo.save(entityToSave)
                originalUpdateTime = savedEntity!!.creationTime
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdCreTimeAndField::class.java)
                val loadedEntity = repo.all().first { it.id == savedEntity!!.id }
                loadedEntity.longField = entityFieldNewValue
            }

            // Assert
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdCreTimeAndField::class.java)
                val loadedEntity = repo.all().first { it.id == savedEntity!!.id }

                assertNotNull(loadedEntity.creationTime, "Creation time should not be null after save")
                println("Original creation time: $originalUpdateTime; new creation time: ${loadedEntity.creationTime}")
                assertEquals(originalUpdateTime, loadedEntity.creationTime, "Creation time should not have changed when data updated")
            }
        }
    }
}
