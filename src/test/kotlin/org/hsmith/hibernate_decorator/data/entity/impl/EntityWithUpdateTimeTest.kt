package org.hsmith.hibernate_decorator.data.entity.impl

import java.time.LocalDateTime
import java.time.ZoneOffset
import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndUpdateTime
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdUpdTimeAndField
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class EntityWithUpdateTimeTest {

    @Test
    fun `GIVEN entity with update time WHEN saved to database THEN entity update time is correctly set`() {
        // Assemble
        var savedEntityId: Long = -1
        val entityToSave = MockEntityWithIdAndUpdateTime()
        var saveTime: LocalDateTime? = null
        assertNull(entityToSave.updateTime, "GIVEN: Update date should be null before test")

        // Act
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndUpdateTime::class.java)
                saveTime = LocalDateTime.now()
                savedEntityId = repo.save(entityToSave).id
            }
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndUpdateTime::class.java)
                val savedEntity = repo.all().first { it.id == savedEntityId }

                // Assert
                assertNotNull(savedEntity.updateTime, "Update time should not be null after save")
                val timeDiff = Math.abs(saveTime!!.toInstant(ZoneOffset.UTC)!!.toEpochMilli() - savedEntity.updateTime!!.toInstant(ZoneOffset.UTC)!!.toEpochMilli())
                assertTrue(timeDiff < 100, "Update time should be within a second of actual save time")
            }
        }
    }

    @Test
    fun `GIVEN entity with update time WHEN change saved to database THEN entity update time is correctly updated`() {
        // Assemble
        val entityFieldNewValue = 200L
        val entityToSave = MockEntityWithIdUpdTimeAndField()
        entityToSave.longField = 100
        var savedEntity: MockEntityWithIdUpdTimeAndField? = null
        var originalUpdateTime: LocalDateTime? = null
        assertNull(entityToSave.updateTime, "GIVEN: Update date should be null before test")

        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdUpdTimeAndField::class.java)
                savedEntity = repo.save(entityToSave)
                originalUpdateTime = savedEntity!!.updateTime
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdUpdTimeAndField::class.java)
                val loadedEntity = repo.all().first { it.id == savedEntity!!.id }
                loadedEntity.longField = entityFieldNewValue
            }

            // Assert
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndUpdateTime::class.java)
                val loadedEntity = repo.all().first { it.id == savedEntity!!.id }

                assertNotNull(loadedEntity.updateTime, "Update time should not be null after save")
                println("Original update time: $originalUpdateTime; new update time: ${loadedEntity.updateTime}")
                assertNotEquals(originalUpdateTime, loadedEntity.updateTime, "Update time should have changed when data updated")
            }
        }
    }
}
