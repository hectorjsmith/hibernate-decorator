package org.hsmith.hibernate_decorator.data.repository.impl

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndLongField
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class RepositoryTest {
    @Test
    fun `GIVEN database with matching entity WHEN single called THEN matching entity returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entitiesToCreate = 5
            val entityValue = 100L
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until entitiesToCreate) {
                    val entity = MockEntityWithIdAndLongField()
                    if (index == entitiesToCreate - 2) {
                        entity.customFieldLong = entityValue
                    }
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val foundEntity = repo.single("customFieldLong", entityValue)

                // Assert
                assertTrue(foundEntity.isPresent, "Expected to find a matching entity")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN single called THEN nothing returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val foundEntity = repo.single("customFieldLong", 100L)

                // Assert
                assertFalse(foundEntity.isPresent, "Did not expect to find a matching entity")
            }
        }
    }

    @Test
    fun `GIVEN database with matching entity WHEN exists called THEN true returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityValue = 100L
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val entity = MockEntityWithIdAndLongField()
                entity.customFieldLong = entityValue
                repo.save(entity)
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val entityExists = repo.exists("customFieldLong", entityValue)

                // Assert
                assertTrue(entityExists, "Expected to find a matching entity")
            }
        }
    }

    @Test
    fun `GIVEN database with matching entity WHEN exists called with manual field name THEN true returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityValue = 100L
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val entity = MockEntityWithIdAndLongField()
                entity.customFieldLong = entityValue
                repo.save(entity)
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val entityExists = repo.exists("customFieldLong", entityValue)

                // Assert
                assertTrue(entityExists, "Expected to find a matching entity")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN exists called THEN false returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val entityExists = repo.exists("customFieldLong", 100L)

                // Assert
                assertFalse(entityExists, "Did not expect matching entity to exist")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN where called THEN empty set returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val foundEntities = repo.where("customFieldLong", 100L)

                // Assert
                assertEquals(0, foundEntities.count(), "Expected found entity list to be empty")
            }
        }
    }

    @Test
    fun `GIVEN database with some matching entities WHEN where called THEN set of all matching results returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entitiesToSave = 5L
            val expectedMatchingEntities = 2L
            val valueToSave = 100L
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until entitiesToSave) {
                    val entity = MockEntityWithIdAndLongField()
                    if (index < expectedMatchingEntities) {
                        entity.customFieldLong = valueToSave
                    }
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val totalEntityCount = repo.count()
                val foundEntities = repo.where("customFieldLong", valueToSave)

                // Assert
                assertEquals(expectedMatchingEntities, foundEntities.count().toLong(),
                        "Expected found entities to have the correct length")
                assertNotEquals(totalEntityCount, foundEntities,
                        "Number of matching entities should not match total number of entities in database")
                for (entity in foundEntities) {
                    assertEquals(valueToSave, entity.customFieldLong, "Expected retrieved entities to have the correct value")
                }
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN delete called THEN nothing happens`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)

                // Assert
                assertEquals(0, repo.count(), "GIVEN: Database should be empty, test is invalid")
                assertDoesNotThrow({ repo.delete("customFieldLong", 100L) },
                        "delete should not throw an exception if no matches found")
            }
        }
    }

    @Test
    fun `GIVEN database with multiple matching entities WHEN delete called THEN correct entity is deleted`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entitiesToSave = 5L
            val expectedMatchingEntities = 2L
            val valueToSave = 100L
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until entitiesToSave) {
                    val entity = MockEntityWithIdAndLongField()
                    if (index < expectedMatchingEntities) {
                        entity.customFieldLong = valueToSave
                    }
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val countBefore = repo.count()
                repo.delete("customFieldLong", valueToSave)
                val countAfter = repo.count()

                // Assert
                assertEquals(entitiesToSave, countBefore,
                        "Expected correct number of entities saved in the database before the delete operation")
                assertEquals(entitiesToSave - expectedMatchingEntities, countAfter,
                        "Number of entities in the database should be correct after deletion")
                for (entity in repo.all()) {
                    assertNotEquals(valueToSave, entity.customFieldLong, "There should be no entities in the database with the queried value")
                }
            }
        }
    }
}
