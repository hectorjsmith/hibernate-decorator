package org.hsmith.hibernate_decorator.data.repository.impl

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithLongId
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class EntityWithIdRepositoryTest {
    @Test
    fun `GIVEN database with matching entity WHEN singleWithId called THEN matching entity returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            var savedEntity: MockEntityWithLongId? = null
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                savedEntity = repo.save(MockEntityWithLongId())
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                val foundEntity = repo.singleWithId(savedEntity!!.id)

                // Assert
                assertTrue(foundEntity.isPresent, "Expected to find entity by ID")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN singleWithId called with an id THEN nothing returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                val foundEntity = repo.singleWithId(1L)

                // Assert
                assertFalse(foundEntity.isPresent, "Did not expect to find any entity by ID")
            }
        }
    }

    @Test
    fun `GIVEN database with matching entity WHEN existsWithId called with ID THEN true returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            var savedEntity: MockEntityWithLongId? = null
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                savedEntity = repo.save(MockEntityWithLongId())
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                val entityExists = repo.existsWithId(savedEntity!!.id)

                // Assert
                assertTrue(entityExists, "Expected entity with ID to exist")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN existsWithId called with an id THEN false returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                val entityExists = repo.existsWithId(1L)

                // Assert
                assertFalse(entityExists, "Did not expect entity with ID to exist")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN allWithIds called with ids THEN empty set returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                val foundEntities = repo.allWithIds(listOf(1L, 2L, 3L, 4L))

                // Assert
                assertEquals(0, foundEntities.count(), "Expected found entity list to be empty")
            }
        }
    }

    @Test
    fun `GIVEN database with some matching entities WHEN allWithIds called with ids THEN set of all matching results returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entitiesToSave = 5L
            val expectedMatchingEntities = 2L
            val savedEntityIdList = mutableListOf<Long>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entitiesToSave) {
                    val savedEntity = repo.save(MockEntityWithLongId())
                    if (index < expectedMatchingEntities) {
                        savedEntityIdList.add(savedEntity.id)
                    }
                }
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                val totalEntityCount = repo.count()
                val foundEntities = repo.allWithIds(savedEntityIdList)

                // Assert
                assertEquals(expectedMatchingEntities, foundEntities.count().toLong(),
                        "Expected found entities to have the correct length")
                assertNotEquals(totalEntityCount, foundEntities,
                        "Number of matching entities should not match total number of entities in database")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN deleteWithId called with ids THEN nothing happens`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)

                // Assert
                assertDoesNotThrow({ repo.deleteWithId(1L) },
                        "Delete by ID when the ID does not exist should not throw an exception")
            }
        }
    }

    @Test
    fun `GIVEN database with matching entity WHEN deleteWithId called with ids THEN correct entity is deleted`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            var savedEntity: MockEntityWithLongId? = null
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                savedEntity = repo.save(MockEntityWithLongId())
            }

            // Act
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                val countBefore = repo.count()
                repo.deleteWithId(savedEntity!!.id)
                val countAfter = repo.count()

                // Assert
                assertEquals(1L, countBefore,
                        "Expected one entity saved in the database before the delete operation")
                assertEquals(0L, countAfter,
                        "No entities expected to be found after the delete operation")
            }
        }
    }
}
