package org.hsmith.hibernate_decorator.query.where.factory

import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator
import org.hsmith.hibernate_decorator.query.where.operator.ClauseOperator
import org.hsmith.hibernate_decorator.query.where.operator.TernaryExpressionOperator
import org.hsmith.hibernate_decorator.query.where.operator.UnaryExpressionOperator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class WhereClauseFactoryImplTest {
    @Test
    fun `GIVEN clause factory WHEN new unary query created THEN correct jpql returned`() {
        // Assemble
        val factory = WhereClauseFactoryImpl()

        // Act
        val query = factory.newWhereClause("id", UnaryExpressionOperator.IS_NULL)

        // Assert
        assertEquals("o.id IS NULL", query.toImmutableWhereClause().jpqlString,
                "Incorrect JPQL query string returned")
    }

    @Test
    fun `GIVEN clause factory WHEN new binary query created THEN correct jpql returned`() {
        // Assemble
        val factory = WhereClauseFactoryImpl()

        // Act
        val query = factory.newWhereClause("id", BinaryExpressionOperator.EQUAL, 15)

        // Assert
        assertEquals("o.id = :param", query.toImmutableWhereClause().jpqlString,
                "Incorrect JPQL query string returned")
    }

    @Test
    fun `GIVEN clause factory WHEN new ternary query created THEN correct jpql returned`() {
        // Assemble
        val factory = WhereClauseFactoryImpl()

        // Act
        val query = factory.newWhereClause("id", TernaryExpressionOperator.BETWEEN, 5, 15)

        // Assert
        assertEquals("o.id BETWEEN :param_a AND :param_b", query.toImmutableWhereClause().jpqlString,
                "Incorrect JPQL query string returned")
    }

    @Test
    fun `GIVEN clause factory WHEN combining two clauses THEN correct jpql returned`() {
        // Assemble
        val factory = WhereClauseFactoryImpl()

        // Act
        val clause1 = factory.newWhereClause("id", UnaryExpressionOperator.IS_NULL)
        val clause2 = factory.newWhereClause("id", TernaryExpressionOperator.BETWEEN, 5, 15)
        val query = factory.newWhereClause(clause1, ClauseOperator.OR, clause2)

        // Assert
        assertEquals("(o.id IS NULL OR o.id BETWEEN :param2_a AND :param2_b)",
                query.toImmutableWhereClause().jpqlString, "Incorrect JPQL query string returned")
    }
}
