package org.hsmith.hibernate_decorator.query.impl

import java.util.*
import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.api.impl.MockDbEntity
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndLongField
import org.hsmith.hibernate_decorator.mock.MockEntityWithLongId
import org.hsmith.hibernate_decorator.query.where.BinaryWhereClause
import org.hsmith.hibernate_decorator.query.where.TernaryWhereClause
import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator
import org.hsmith.hibernate_decorator.query.where.operator.TernaryExpressionOperator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class QueryImplTest {
    @Test
    fun `GIVEN query object WHEN created THEN where clause is empty`() {

        // Assemble
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockDbEntity::class.java)

                // Act
                val query = repo.newQuery()

                // Assert
                assertTrue(query.whereClause.isEmpty,
                        "Where clause on the query object should be empty on creation")
            }
        }
    }

    @Test
    fun `GIVEN query object WHEN single clause added THEN where clause has single condition`() {

        // Assemble
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockDbEntity::class.java)

                // Act
                val query = repo.newQuery()
                query.where("id", 15)

                // Assert
                assertTrue(query.whereClause.isPresent, "Where clause should be present")
                assertEquals("o.id = :param", query.whereClause.get().jpqlString,
                        "Query where clause should match expected")
            }
        }
    }

    @Test
    fun `GIVEN query object WHEN multiple clauses added THEN clauses are combined using and`() {

        // Assemble
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val repo = session.newRepository(MockDbEntity::class.java)

                // Act
                val query = repo.newQuery()
                query.where("id", 15)
                query.where("field1", 30)
                query.where("field2", 45)
                query.where("field3", 60)

                // Assert
                assertTrue(query.whereClause.isPresent, "Where clause should be present")
                assertEquals("(((o.id = :param111 AND o.field1 = :param112) AND o.field2 = :param12) AND o.field3 = :param2)",
                        query.whereClause.get().jpqlString, "Query where clause should match expected")
            }
        }
    }

    @Test
    fun `GIVEN query object WHEN deleting entities with based on a query THEN entities are correctly deleted`() {

        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount = 10L
            val entityList = mutableListOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
            }

            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)

                // Act
                val query = repo.newQuery()
                val initialCount = query.count()

                query.where(TernaryWhereClause("id", TernaryExpressionOperator.BETWEEN, 2L, 7L))
                val matchCount = query.count()
                val deleteCount = query.delete()

                // Assert
                assertEquals(entityCount, initialCount, "GIVEN: Number of entities in database does not match expected")
                assertEquals(matchCount, deleteCount, "Number of deleted entities should match number of matches")

                assertEquals(entityCount - deleteCount, repo.count(), "Number of remaining entities should match expected")
            }
        }
    }

    @Test
    fun `GIVEN query object WHEN deleting all entities THEN entities are correctly deleted`() {

        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount = 10L
            val entityList = mutableListOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
            }

            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)

                // Act
                val query = repo.newQuery()
                val deleteCount = query.delete()

                // Assert
                assertEquals(entityCount, deleteCount, "All entities in database should be deleted")
                assertEquals(0, repo.count(), "No entities should remain in the database")
            }
        }
    }

    @Test
    fun `GIVEN database with matching entities WHEN select query with limit executed THEN correct entities returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 10L
            val matchingEntityCount = 6L
            val queryLimit = 4

            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    entity.customFieldLong = index
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong",
                        BinaryExpressionOperator.LESS_THAN,
                        matchingEntityCount)

                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                val queryResultWithLimit = repo
                        .newQuery()
                        .where(whereClause)
                        .limit(queryLimit)
                        .all()

                val queryResultWithoutLimit = repo
                        .newQuery()
                        .where(whereClause)
                        .all()

                // Assert
                assertNotEquals(queryLimit, queryResultWithoutLimit.count(), "Number of entities that match query should not equal limit")
                assertEquals(queryLimit, queryResultWithLimit.count(), "Expected found entity count to match query limit")
            }
        }
    }
}
