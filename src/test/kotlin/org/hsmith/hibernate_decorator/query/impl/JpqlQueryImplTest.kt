package org.hsmith.hibernate_decorator.query.impl

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithLongId
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class JpqlQueryImplTest {
    @Test
    fun `GIVEN raw jpql query WHEN raw delete query executed THEN query is correctly executed`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount = 10L
            val entityList = mutableListOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
            }

            api.newSession().use { session ->
                // Act
                val jpqlQuery = session.newUpdateOnlyJpqlQuery("DELETE FROM ${MockEntityWithLongId::class.java.simpleName}")
                val updateCount = jpqlQuery.executeUpdate().toLong()

                // Assert
                assertEquals(entityCount, updateCount, "All entities in database should be deleted")
                assertEquals(0, session.newRepository(MockEntityWithLongId::class.java).count(),
                        "No more entities should exist in the database")
            }
        }
    }

    @Test
    fun `GIVEN raw jpql query WHEN raw select query executed THEN correct results returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount = 10L
            val entityList = mutableListOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
            }

            api.newSession().use { session ->
                // Act
                val jpqlQuery = session.newJpqlQuery(
                        MockEntityWithLongId::class.java,
                        "SELECT o FROM ${MockEntityWithLongId::class.java.simpleName} o WHERE o.id < :id",
                        mapOf(Pair("id", 5L)))
                val returnedEntityList = jpqlQuery.resultList()

                // Assert
                assertEquals(4, returnedEntityList.size, "Correct number of entities should be returned")
            }
        }
    }

    @Test
    fun `GIVEN raw jpql query WHEN raw select query with limit executed THEN correct results returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount = 10L
            val limit = 2
            val entityList = mutableListOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
            }

            api.newSession().use { session ->
                // Act
                val jpqlQuery = session.newJpqlQuery(
                        MockEntityWithLongId::class.java,
                        "SELECT o FROM ${MockEntityWithLongId::class.java.simpleName} o WHERE o.id < :id",
                        mapOf(Pair("id", 5L)))
                val resultsWithLimit = jpqlQuery.resultList(limit)
                val resultsWithoutLimit = jpqlQuery.resultList()

                // Assert
                assertNotEquals(limit, resultsWithoutLimit.size, "Number of matching entities should not match limit")
                assertEquals(limit, resultsWithLimit.size, "Number of returned entities should match set limit")
            }
        }
    }
}
