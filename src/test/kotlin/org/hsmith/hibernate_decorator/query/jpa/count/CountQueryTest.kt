package org.hsmith.hibernate_decorator.query.jpa.count

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockBaseEntity
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndLongField
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndStringField
import org.hsmith.hibernate_decorator.mock.MockEntityWithLongId
import org.hsmith.hibernate_decorator.query.where.BinaryWhereClause
import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class CountQueryTest {
    @Test
    fun `GIVEN empty database WHEN count called THEN count returns 0`() {
        // Assemble
        val expectedResult: Long = 0

        // Act
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            api.newSession().use { session ->
                val query: CountQueryDecorator<MockEntityWithLongId> = CountQueryImpl(session.entityManagerDecorator, MockEntityWithLongId::class.java)
                val count = query.count()

                // Assert
                assertEquals(expectedResult, count, "countAll should return 0 on an empty database")
            }
        }
    }

    @Test
    fun `GIVEN database with 5 elements WHEN count called THEN count returns 5`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount: Long = 5
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                assertEquals(0, repo.count(), "GIVEN: Database should be empty before test")

                for (i: Long in 0 until entityCount) {
                    repo.save(MockEntityWithLongId())
                }
            }

            // Act
            api.newSession().use { session ->
                val query: CountQueryDecorator<MockEntityWithLongId> = CountQueryImpl(session.entityManagerDecorator, MockEntityWithLongId::class.java)
                val count = query.count()

                // Assert
                assertEquals(entityCount, count, "countAll should return the correct number of elements in the database")
            }
        }
    }

    @Test
    fun `GIVEN database with 5 instances of two sub-types WHEN count called with super-type THEN count returns count of both sub-types combined`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount: Long = 5
            api.newSession().use { session ->
                val repoForEntityA = session.newRepository(MockEntityWithIdAndLongField::class.java)
                assertEquals(0, repoForEntityA.count(), "GIVEN: Database should not contain any instances of 'MockEntityWithIdAndLongField' before test")

                for (i: Long in 0 until entityCount) {
                    repoForEntityA.save(MockEntityWithIdAndLongField())
                }

                val repoForEntityB = session.newRepository(MockEntityWithIdAndStringField::class.java)
                assertEquals(0, repoForEntityB.count(), "GIVEN: Database should not contain any instances of 'MockEntityWithIdAndStringField' before test")

                for (i: Long in 0 until entityCount) {
                    repoForEntityB.save(MockEntityWithIdAndStringField())
                }
            }

            // Act
            api.newSession().use { session ->
                val query: CountQueryDecorator<MockBaseEntity> = CountQueryImpl(session.entityManagerDecorator, MockBaseEntity::class.java)
                val count = query.count()

                // Assert
                assertEquals(entityCount * 2, count,
                        "countAll should return the total number of elements in the database, including sub-types")
            }
        }
    }

    @Test
    fun `GIVEN database with matching entities WHEN count where field matches THEN correct number of results returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 5L
            val matchingEntityCount = 3L
            val fieldValueToFind = 10L

            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    if (index < matchingEntityCount) {
                        entity.customFieldLong = fieldValueToFind
                    } else {
                        entity.customFieldLong = fieldValueToFind + 15L
                    }
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong", BinaryExpressionOperator.EQUAL, fieldValueToFind)
                val query: CountQueryImpl<MockEntityWithIdAndLongField> =
                        CountQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)
                val queryResult = query.count(whereClause)

                // Assert
                assertEquals(matchingEntityCount, queryResult, "Number of entities found by query should match number in database")
            }
        }
    }
}
