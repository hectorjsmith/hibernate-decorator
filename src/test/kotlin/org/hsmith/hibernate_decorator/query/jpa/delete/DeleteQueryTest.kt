package org.hsmith.hibernate_decorator.query.jpa.delete

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithLongId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DeleteQueryTest {
    @Test
    fun `GIVEN database with a single entity WHEN entity deleted THEN database becomes empty`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            var dbEntity = MockEntityWithLongId()

            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                dbEntity = repo.save(dbEntity)
                Assertions.assertEquals(1, repo.count(), "GIVEN: Database should have a single entity before test")
            }

            // Act
            api.newSession().use { session ->
                val query: DeleteQueryImpl<MockEntityWithLongId> = DeleteQueryImpl(session.entityManagerDecorator)
                query.delete(dbEntity)

                // Assert
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(0, repo.count(), "Database should now be empty")
            }
        }
    }

    @Test
    fun `GIVEN database with a multiple entities WHEN entity deleted THEN only a single entity deleted`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            var dbEntity = MockEntityWithLongId()

            val entityCount: Long = 5
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                dbEntity = repo.save(dbEntity)

                for (i: Long in 0 until entityCount) {
                    repo.save(MockEntityWithLongId())
                }
                Assertions.assertEquals(entityCount + 1, repo.count(),
                        "GIVEN: Database should have the correct number of entities before the test")
            }

            // Act
            api.newSession().use { session ->
                val query: DeleteQueryImpl<MockEntityWithLongId> = DeleteQueryImpl(session.entityManagerDecorator)
                query.delete(dbEntity)

                // Assert
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(entityCount, repo.count(),
                        "Database should still contain all existing entities except the deleted item")
            }
        }
    }

    @Test
    fun `GIVEN database with a multiple entities WHEN multiple entities deleted THEN other enities are not deleted`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount: Long = 10
            val entityList = mutableListOf<MockEntityWithLongId>()
            var entityListToDelete = listOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (i: Long in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
                entityListToDelete = entityList.filter { it.id % 2 == 0L }

                Assertions.assertEquals(entityCount, repo.count(),
                        "GIVEN: Database should have the correct number of entities before the test")
                Assertions.assertTrue(entityListToDelete.size > 0,
                        "GIVEN: At least one entity should be queued for deletion")
            }

            // Act
            api.newSession().use { session ->
                val query: DeleteQueryImpl<MockEntityWithLongId> = DeleteQueryImpl(session.entityManagerDecorator)
                query.deleteAll(entityListToDelete)

                // Assert
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(entityCount - entityListToDelete.size, repo.count(),
                        "Database should still contain all existing entities except the deleted item")

                for (entity in repo.all()) {
                    Assertions.assertFalse(entityListToDelete.any { it.id == entity.id },
                            "Database should not contain any entity that was queued for deletion")
                }
            }
        }
    }
}
