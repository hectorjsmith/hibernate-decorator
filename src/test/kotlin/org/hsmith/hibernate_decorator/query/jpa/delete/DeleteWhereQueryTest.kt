package org.hsmith.hibernate_decorator.query.jpa.delete

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndLongField
import org.hsmith.hibernate_decorator.query.jpa.count.CountQueryImpl
import org.hsmith.hibernate_decorator.query.where.BinaryWhereClause
import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class DeleteWhereQueryTest {
    @Test
    fun `GIVEN database with multiple matching entities WHEN delete where field matches THEN correct number of entities deleted`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 5L
            val matchingEntityCount = 3L
            val fieldValueToFind = 10L
            val nonMatchingEntityCount = totalEntityCount - matchingEntityCount

            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    if (index < matchingEntityCount) {
                        entity.customFieldLong = fieldValueToFind
                    } else {
                        entity.customFieldLong = fieldValueToFind + 15L
                    }
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong", BinaryExpressionOperator.EQUAL, fieldValueToFind)
                val query: DeleteWhereQueryDecorator<MockEntityWithIdAndLongField> =
                        DeleteWhereQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)

                val entitiesDeleted = query.deleteAllWhere(whereClause)

                // Assert
                assertEquals(matchingEntityCount, entitiesDeleted.toLong(), "Number of entities deleted should match number of matching entities")

                val countQuery = CountQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)
                val remainingEntityCount = countQuery.count()

                assertEquals(nonMatchingEntityCount, remainingEntityCount, "Number of entities remaining in the database should match expected")
            }
        }
    }

    @Test
    fun `GIVEN database with no matching entities WHEN delete where field matches THEN zero returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 5L
            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    entity.customFieldLong = 100L
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong", BinaryExpressionOperator.EQUAL, 0L)
                val query: DeleteWhereQueryDecorator<MockEntityWithIdAndLongField> =
                        DeleteWhereQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)

                val entitiesDeleted = query.deleteAllWhere(whereClause)

                // Assert
                assertEquals(0, entitiesDeleted, "No entities should have been deleted when no matches exist")
            }
        }
    }
}
