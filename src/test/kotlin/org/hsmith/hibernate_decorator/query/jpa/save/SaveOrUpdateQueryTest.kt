package org.hsmith.hibernate_decorator.query.jpa.save

import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndLongField
import org.hsmith.hibernate_decorator.mock.MockEntityWithLongId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class SaveOrUpdateQueryTest {
    @Test
    fun `GIVEN empty database WHEN entity saved THEN contains one element`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(0, repo.count(), "GIVEN: Database should be empty before test")
            }

            // Act
            api.newSession().use { session ->
                val query: SaveOrUpdateQueryImpl<MockEntityWithLongId> = SaveOrUpdateQueryImpl(session.entityManagerDecorator)
                query.save(MockEntityWithLongId())

                // Assert
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(1, repo.count(), "Database should have a single entity")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN multiple entities saved THEN contains correct number of elements`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount: Long = 5
            val entityList = mutableListOf<MockEntityWithLongId>()
            for (index in 0 until entityCount) {
                entityList.add(MockEntityWithLongId())
            }

            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(0, repo.count(), "GIVEN: Database should be empty before test")
            }

            // Act
            api.newSession().use { session ->
                val query: SaveOrUpdateQueryImpl<MockEntityWithLongId> = SaveOrUpdateQueryImpl(session.entityManagerDecorator)
                query.saveAll(entityList)

                // Assert
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(entityCount, repo.count(), "Database should have correct number of elements")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN entity is saved THEN returned object has ID set`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityToSave = MockEntityWithLongId()

            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(0, repo.count(), "GIVEN: Database should be empty before test")
                Assertions.assertEquals(0, entityToSave.id, "GIVEN: Entity ID should be 0 before start of test")
            }

            // Act
            api.newSession().use { session ->
                val query: SaveOrUpdateQueryImpl<MockEntityWithLongId> = SaveOrUpdateQueryImpl(session.entityManagerDecorator)
                val savedEntity = query.save(entityToSave)

                // Assert
                Assertions.assertEquals(1, savedEntity.id, "Saved element should have the correct ID")
            }
        }
    }

    @Test
    fun `GIVEN database WHEN entity changed and saved THEN entity is updated and not added`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val newCustomFieldValue: Long = 100
            var dbEntity = MockEntityWithIdAndLongField()
            dbEntity.customFieldLong = 0

            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                dbEntity = repo.save(dbEntity)
                Assertions.assertEquals(1, repo.count(), "GIVEN: Database should have a single entity before test")
            }

            // Act
            var savedDbEntity: MockEntityWithIdAndLongField? = null
            api.newSession().use { session ->
                dbEntity.customFieldLong = newCustomFieldValue

                val query: SaveOrUpdateQueryImpl<MockEntityWithIdAndLongField> = SaveOrUpdateQueryImpl(session.entityManagerDecorator)
                savedDbEntity = query.save(dbEntity)
            }

            // Assert
            api.newSession().use { session ->
                Assertions.assertEquals(newCustomFieldValue, savedDbEntity!!.customFieldLong, "Saved entity should have the correct value")
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                Assertions.assertEquals(1, repo.count(), "Database should still have a single item")
            }
        }
    }
}
