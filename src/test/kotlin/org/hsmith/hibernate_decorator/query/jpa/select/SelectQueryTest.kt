package org.hsmith.hibernate_decorator.query.jpa.select

import java.util.*
import javax.persistence.NoResultException
import javax.persistence.NonUniqueResultException
import org.hsmith.hibernate_decorator.api.HibernateDecoratorApi
import org.hsmith.hibernate_decorator.mock.MockBaseEntity
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndLongField
import org.hsmith.hibernate_decorator.mock.MockEntityWithIdAndStringField
import org.hsmith.hibernate_decorator.mock.MockEntityWithLongId
import org.hsmith.hibernate_decorator.query.where.BinaryWhereClause
import org.hsmith.hibernate_decorator.query.where.CompositeWhereClause
import org.hsmith.hibernate_decorator.query.where.TernaryWhereClause
import org.hsmith.hibernate_decorator.query.where.UnaryWhereClause
import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator
import org.hsmith.hibernate_decorator.query.where.operator.ClauseOperator
import org.hsmith.hibernate_decorator.query.where.operator.TernaryExpressionOperator
import org.hsmith.hibernate_decorator.query.where.operator.UnaryExpressionOperator
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

class SelectQueryTest {
    @Test
    fun `GIVEN an empty database WHEN find all entities THEN empty list returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(0, repo.count(), "GIVEN: Database should be empty before test")
            }

            // Act
            api.newSession().use { session ->
                val query: SelectQueryImpl<MockEntityWithLongId> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithLongId::class.java)
                val queryResults = query.selectAll(Optional.empty())

                // Assert
                Assertions.assertEquals(listOf<MockEntityWithLongId>(), queryResults, "Select all should return an empty list")
            }
        }
    }

    @Test
    fun `GIVEN an empty database WHEN find single THEN exception thrown`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                Assertions.assertEquals(0, repo.count(), "GIVEN: Database should be empty before test")
            }

            // Act / Assert
            api.newSession().use { session ->
                val query: SelectQueryImpl<MockEntityWithLongId> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithLongId::class.java)

                assertThrows<NoResultException>("Exception expected when no entities found") { query.selectUnique() }
            }
        }
    }

    @Test
    fun `GIVEN database with elements WHEN find all entities THEN list of all entities returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount = 5
            val entityList = mutableListOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
            }

            // Act
            api.newSession().use { session ->
                val query: SelectQueryImpl<MockEntityWithLongId> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithLongId::class.java)
                val queryResults = query.selectAll(Optional.empty())

                // Assert
                Assertions.assertEquals(entityCount, queryResults.count(),
                        "Select all should return a list of all entities")
                Assertions.assertEquals(entityList.map { it.id }, queryResults.map { it.id },
                        "All database entities should exist in query result list")
            }
        }
    }

    @Test
    fun `GIVEN database with multiple matching elements WHEN find single THEN exception thrown`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount = 5
            val entityList = mutableListOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
            }

            // Act / Assert
            api.newSession().use { session ->
                val query: SelectQueryImpl<MockEntityWithLongId> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithLongId::class.java)

                assertThrows<NonUniqueResultException>("Exception expected when multiple matching entities found") { query.selectUnique() }
            }
        }
    }

    @Test
    fun `GIVEN database with single matching element WHEN find single THEN matching entity returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount = 5
            val entityList = mutableListOf<MockEntityWithLongId>()
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithLongId::class.java)
                for (index in 0 until entityCount) {
                    entityList.add(repo.save(MockEntityWithLongId()))
                }
            }

            // Act
            api.newSession().use { session ->
                val query: SelectQueryImpl<MockEntityWithLongId> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithLongId::class.java)

                val idToFind = 3L
                val clause = BinaryWhereClause("id", BinaryExpressionOperator.EQUAL, idToFind)
                val queryResult = query.selectUniqueWhere(clause)

                // Assert
                assertEquals(idToFind, queryResult.id, "Entity ID should match expected")
            }
        }
    }

    @Test
    fun `GIVEN database with 5 instances of two sub-types WHEN selectAll called with super-type THEN selectAll returns instances of both sub-types`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Assemble
            val entityCount: Long = 5
            api.newSession().use { session ->
                val repoForEntityA = session.newRepository(MockEntityWithIdAndLongField::class.java)
                Assertions.assertEquals(0, repoForEntityA.count(), "GIVEN: Database should not contain any instances of 'MockEntityWithIdAndLongField' before test")

                for (i: Long in 0 until entityCount) {
                    repoForEntityA.save(MockEntityWithIdAndLongField())
                }

                val repoForEntityB = session.newRepository(MockEntityWithIdAndStringField::class.java)
                Assertions.assertEquals(0, repoForEntityB.count(), "GIVEN: Database should not contain any instances of 'MockEntityWithIdAndStringField' before test")

                for (i: Long in 0 until entityCount) {
                    repoForEntityB.save(MockEntityWithIdAndStringField())
                }
            }

            // Act
            api.newSession().use { session ->
                val query: SelectQueryDecorator<MockBaseEntity> = SelectQueryImpl(session.entityManagerDecorator, MockBaseEntity::class.java)
                val entityList = query.selectAll(Optional.empty())

                // Assert
                Assertions.assertEquals(entityCount * 2, entityList.count().toLong(),
                        "Number of entities returned by selectAll should match total number of entities of both sub-types")
            }
        }
    }

    @Test
    fun `GIVEN database with multiple entities WHEN select where field matches THEN correct entities returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 5L
            val matchingEntityCount = 3L
            val fieldValueToFind = 10L

            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    if (index < matchingEntityCount) {
                        entity.customFieldLong = fieldValueToFind
                    } else {
                        entity.customFieldLong = fieldValueToFind + 15L
                    }
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong", BinaryExpressionOperator.EQUAL, fieldValueToFind)
                val query: SelectQueryImpl<MockEntityWithIdAndLongField> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)
                val queryResults = query.selectAllWhere(whereClause, Optional.empty())

                // Assert
                Assertions.assertEquals(matchingEntityCount, queryResults.count().toLong(), "Number of results returned does not match expected")
                queryResults.forEach {
                    Assertions.assertEquals(fieldValueToFind, it.customFieldLong, "Entity returned does not have the correct field value")
                }
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN select all where THEN empty set returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong", BinaryExpressionOperator.EQUAL, 1L)
                val query: SelectQueryImpl<MockEntityWithIdAndLongField> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)

                val results = query.selectAllWhere(whereClause, Optional.empty())

                // Assert
                Assertions.assertEquals(0, results.count(),
                        "Find all should return no results when database is empty")
            }
        }
    }

    @Test
    fun `GIVEN empty database WHEN select unique where THEN exception thrown`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong", BinaryExpressionOperator.EQUAL, 1L)
                val query: SelectQueryImpl<MockEntityWithIdAndLongField> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)

                // Assert
                Assertions.assertThrows(NoResultException::class.java, { query.selectUniqueWhere(whereClause) },
                        "Find unique should throw an exception when no result found")
            }
        }
    }

    @Test
    fun `GIVEN database with multiple matching entities WHEN select unique where field matches THEN exception thrown`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 5
            val matchingEntityCount = 2
            val fieldValueToFind = 10L

            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    if (index < matchingEntityCount) {
                        entity.customFieldLong = fieldValueToFind
                    } else {
                        entity.customFieldLong = fieldValueToFind + 15L
                    }
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong", BinaryExpressionOperator.EQUAL, fieldValueToFind)
                val query: SelectQueryImpl<MockEntityWithIdAndLongField> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)

                // Assert
                Assertions.assertThrows(NonUniqueResultException::class.java, { query.selectUniqueWhere(whereClause) },
                        "An exception should be thrown when multiple entities match the given query")
            }
        }
    }

    @Test
    fun `GIVEN database with single matching entity WHEN select unique where field matches THEN correct entity returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 5L
            val matchingEntityCount = 1L
            val fieldValueToFind = 10L

            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    if (index < matchingEntityCount) {
                        entity.customFieldLong = fieldValueToFind
                    } else {
                        entity.customFieldLong = fieldValueToFind + 15L
                    }
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong", BinaryExpressionOperator.EQUAL, fieldValueToFind)
                val query: SelectQueryImpl<MockEntityWithIdAndLongField> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)
                val queryResult = query.selectUniqueWhere(whereClause)

                // Assert
                Assertions.assertEquals(fieldValueToFind, queryResult.customFieldLong, "Entity returned does not have the correct field value")
            }
        }
    }

    @Test
    fun `GIVEN database with matching entities WHEN select where with composite query THEN correct entities returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 10L
            val matchingEntityCount = 4L
            val minFieldValueToFind = 2L
            val maxFieldValueToFind = 5L

            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    entity.customFieldLong = index
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = CompositeWhereClause(
                        UnaryWhereClause("customFieldLong", UnaryExpressionOperator.IS_NOT_EMPTY),
                        ClauseOperator.AND,
                        TernaryWhereClause("customFieldLong", TernaryExpressionOperator.BETWEEN, minFieldValueToFind, maxFieldValueToFind))

                val query: SelectQueryImpl<MockEntityWithIdAndLongField> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)
                val queryResult = query.selectAllWhere(whereClause, Optional.empty())

                // Assert
                Assertions.assertEquals(matchingEntityCount, queryResult.count().toLong(), "Expected found entity count to match expected")
                for (result in queryResult) {
                    Assertions.assertTrue(result.customFieldLong in minFieldValueToFind..maxFieldValueToFind,
                        "Field values on the returned results should be within the correct range")
                }
            }
        }
    }

    @Test
    fun `GIVEN database with matching entities WHEN select with limit THEN correct entities returned`() {
        HibernateDecoratorApi.newDbApiBuilder().build().use { api ->
            val totalEntityCount = 10L
            val matchingEntityCount = 6L
            val queryLimit = Optional.of(4)

            // Assemble
            api.newSession().use { session ->
                val repo = session.newRepository(MockEntityWithIdAndLongField::class.java)
                for (index in 0 until totalEntityCount) {
                    val entity = MockEntityWithIdAndLongField()
                    entity.customFieldLong = index
                    repo.save(entity)
                }
            }

            // Act
            api.newSession().use { session ->
                val whereClause = BinaryWhereClause("customFieldLong",
                        BinaryExpressionOperator.LESS_THAN,
                        matchingEntityCount)

                val query: SelectQueryImpl<MockEntityWithIdAndLongField> =
                        SelectQueryImpl(session.entityManagerDecorator, MockEntityWithIdAndLongField::class.java)
                val queryResultWithLimit = query.selectAllWhere(whereClause, queryLimit)
                val queryResultWithoutLimit = query.selectAllWhere(whereClause, Optional.empty())

                // Assert
                assertNotEquals(queryLimit.get(), queryResultWithoutLimit.count(), "Number of entities that match query should not equal limit")
                assertEquals(queryLimit.get(), queryResultWithLimit.count(), "Expected found entity count to match query limit")
            }
        }
    }
}
