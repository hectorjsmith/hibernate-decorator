package org.hsmith.hibernate_decorator.data.repository.impl

import java.util.*
import org.hsmith.hibernate_decorator.data.entity.EntityWithId
import org.hsmith.hibernate_decorator.data.repository.EntityRepository
import org.hsmith.hibernate_decorator.data.repository.EntityWithIdRepository
import org.hsmith.hibernate_decorator.query.where.factory.WhereClauseFactory
import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class EntityWithIdRepositoryImpl<TEntity : EntityWithId<TId>, TId : Any>(
    private val entityManagerDecorator: EntityManagerDecorator,
    private val handledType: Class<TEntity>,
    private val whereClauseFactory: WhereClauseFactory,
    private val delegate: EntityRepository<TEntity>
) : EntityRepository<TEntity> by delegate, EntityWithIdRepository<TEntity, TId> {

    private val idFieldName = EntityWithId<Any>::id.name

    override fun singleWithId(id: TId): Optional<TEntity> {
        return delegate.single(idFieldName, id)
    }

    override fun existsWithId(id: TId): Boolean {
        return delegate.exists(idFieldName, id)
    }

    override fun allWithIds(ids: Iterable<TId>): Iterable<TEntity> {
        return delegate
                .newQuery()
                .where(whereClauseFactory.newWhereClause(idFieldName, BinaryExpressionOperator.IN, ids.toList()))
                .all()
    }

    override fun deleteWithId(id: TId): Long {
        return delegate.delete(idFieldName, id)
    }
}
