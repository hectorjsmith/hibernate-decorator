package org.hsmith.hibernate_decorator.data.repository.impl

import org.hsmith.hibernate_decorator.data.entity.Entity
import org.hsmith.hibernate_decorator.data.repository.EntityRepository
import org.hsmith.hibernate_decorator.data.repository.Repository

class EntityRepositoryImpl<TEntity : Entity>(
    private val delegate: Repository<TEntity>
) : Repository<TEntity> by delegate, EntityRepository<TEntity>
