package org.hsmith.hibernate_decorator.data.repository.impl

import java.util.*
import org.hsmith.hibernate_decorator.data.repository.Repository
import org.hsmith.hibernate_decorator.query.Query
import org.hsmith.hibernate_decorator.query.impl.QueryImpl
import org.hsmith.hibernate_decorator.query.jpa.delete.DeleteQueryImpl
import org.hsmith.hibernate_decorator.query.jpa.save.SaveOrUpdateQueryImpl
import org.hsmith.hibernate_decorator.query.where.factory.WhereClauseFactory
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class RepositoryImpl<TEntity : Any>(
    private val entityManagerDecorator: EntityManagerDecorator,
    private val handledType: Class<TEntity>,
    private val whereClauseFactory: WhereClauseFactory
) : Repository<TEntity> {

    override fun <TEntityToSave : TEntity> save(entity: TEntityToSave): TEntityToSave {
        val query = SaveOrUpdateQueryImpl<TEntity>(entityManagerDecorator)
        return query.save(entity)
    }

    override fun <TEntityToSave : TEntity> save(entities: Iterable<TEntityToSave>): Iterable<TEntityToSave> {
        val query = SaveOrUpdateQueryImpl<TEntity>(entityManagerDecorator)
        return query.saveAll(entities)
    }

    override fun newQuery(): Query<TEntity> {
        return QueryImpl(entityManagerDecorator, handledType, whereClauseFactory)
    }

    override fun exists(): Boolean {
        return newQuery().exists()
    }

    override fun exists(fieldName: String, value: Any): Boolean {
        return newQuery()
                .where(fieldName, value)
                .exists()
    }

    override fun all(): Iterable<TEntity> {
        return newQuery().all()
    }

    override fun where(fieldName: String, value: Any): Iterable<TEntity> {
        return newQuery()
                .where(fieldName, value)
                .all()
    }

    override fun single(fieldName: String, value: Any): Optional<TEntity> {
        return newQuery()
                .where(fieldName, value)
                .single()
    }

    override fun count(): Long {
        return newQuery().count()
    }

    override fun count(fieldName: String, value: Any): Long {
        return newQuery()
                .where(fieldName, value)
                .count()
    }

    override fun delete(entity: TEntity) {
        val query = DeleteQueryImpl<TEntity>(entityManagerDecorator)
        query.delete(entity)
    }

    override fun delete(entities: Iterable<TEntity>) {
        val query = DeleteQueryImpl<TEntity>(entityManagerDecorator)
        query.deleteAll(entities)
    }

    override fun delete(fieldName: String, value: Any): Long {
        return newQuery()
                .where(fieldName, value)
                .delete()
    }

    override fun clear() {
        newQuery().delete()
    }
}
