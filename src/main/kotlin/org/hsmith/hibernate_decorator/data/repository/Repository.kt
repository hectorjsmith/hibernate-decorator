package org.hsmith.hibernate_decorator.data.repository

import java.util.*
import javax.persistence.NonUniqueResultException
import org.hsmith.hibernate_decorator.api.DbSession
import org.hsmith.hibernate_decorator.query.Query

/**
 * Generic repository class that is responsible for database IO.
 *
 * The repository includes several methods to read and write data to the database.
 * This interface can be extended to provide extra methods for specific entity types.
 *
 * Note that while this repository allows any entity type, some types may not be supported by the underlying database.
 * See [DbSession.newRepository] for how to create a repository instance.
 */
interface Repository<TEntity : Any> {
    /**
     * Saves a given entity. Use the returned instance for further operations as the save operation might have changed the
     * entity instance completely.
     *
     * @param entity must not be null.
     * @return the saved entity; will never be null.
     * @throws IllegalArgumentException in case the given entity is null.
     */
    fun <TEntityToSave : TEntity> save(entity: TEntityToSave): TEntityToSave

    /**
     * Saves all given entities.
     *
     * @param entities must not be null nor must it contain null.
     * @return the saved entities; will never be null. The returned Iterable will have the same size
     * as the Iterable passed as an argument.
     * @throws IllegalArgumentException in case the given [entities][Iterable] or one of its entities is
     * null.
     */
    fun <TEntityToSave : TEntity> save(entities: Iterable<TEntityToSave>): Iterable<TEntityToSave>

    /**
     * Build a new query object to run more complex queries.
     */
    fun newQuery(): Query<TEntity>

    /**
     * Check if there are any entities in this repository.
     *
     * @return true if any entity is found. False otherwise.
     */
    fun exists(): Boolean

    /**
     * Check if there are any entities in this repository where the value of "fieldName" matches the provided value.
     *
     * @return true if any entity found. False otherwise.
     */
    fun exists(fieldName: String, value: Any): Boolean

    /**
     * Returns all instances of the type.
     *
     * @return all entities.
     */
    fun all(): Iterable<TEntity>

    /**
     * Return all instances of the type where the value of "fieldName" matches the provided value.
     *
     * @return all entities where the field value matches.
     */
    fun where(fieldName: String, value: Any): Iterable<TEntity>

    /**
     * Find a single unique entity where the value of "fieldName" matches the provided value.
     * If no entity matches, the return object will be empty.
     * If more than one entity matches, an exception is thrown.
     *
     * @return The only entity that matches. If no entity matches, the optional object will be empty.
     * @throws NonUniqueResultException if more than one matching entity is found.
     */
    fun single(fieldName: String, value: Any): Optional<TEntity>

    /**
     * Returns the number of entities available.
     *
     * @return the number of entities.
     */
    fun count(): Long

    /**
     * Returns the number of entities where the value of "fieldName" matches the provided value.
     *
     * @return the number of entities where the field value matches.
     */
    fun count(fieldName: String, value: Any): Long

    /**
     * Deletes a given entity.
     *
     * @param entity must not be null.
     * @throws IllegalArgumentException in case the given entity is null.
     */
    fun delete(entity: TEntity)

    /**
     * Deletes the given entities.
     *
     * @param entities must not be null. Must not contain null elements.
     * @throws IllegalArgumentException in case the given entities or one of its entities is null.
     */
    fun delete(entities: Iterable<TEntity>)

    /**
     * Deletes all entities where the value of "fieldName" matches the provided value.
     *
     * @return the number of entities deleted by this operation.
     */
    fun delete(fieldName: String, value: Any): Long

    /**
     * Deletes all entities managed by the repository.
     */
    fun clear()
}
