package org.hsmith.hibernate_decorator.data.repository

import org.hsmith.hibernate_decorator.data.entity.Entity

/**
 * Database entity repository.
 * This interface extends [Repository] to add extra database IO methods for database entities that extend [Entity].
 *
 * See [Repository].
 */
interface EntityRepository<TEntity : Entity> : Repository<TEntity>
