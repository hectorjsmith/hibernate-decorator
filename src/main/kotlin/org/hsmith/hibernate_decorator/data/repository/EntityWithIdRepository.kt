package org.hsmith.hibernate_decorator.data.repository

import java.util.*
import org.hsmith.hibernate_decorator.data.entity.EntityWithId

/**
 * Database entity repository.
 * This interface extends [EntityRepository] to add extra database IO methods for database entities that extend
 * the [EntityWithId] interface.
 * For example, this repository adds methods to find entities by ID.
 *
 * See [EntityRepository] and [Repository].
 */
interface EntityWithIdRepository<TEntity : EntityWithId<TId>, TId : Any> : EntityRepository<TEntity> {
    /**
     * Retrieves an entity by its id.
     *
     * @param id must not be null.
     * @return the entity with the given id or Optional#empty() if none found.
     * @throws IllegalArgumentException if id is null.
     */
    fun singleWithId(id: TId): Optional<TEntity>

    /**
     * Returns whether an entity with the given id exists.
     *
     * @param id must not be null.
     * @return true if an entity with the given id exists, false otherwise.
     * @throws IllegalArgumentException if id is null.
     */
    fun existsWithId(id: TId): Boolean

    /**
     * Returns all instances of the type `TEntity` with the given IDs.
     * If some or all ids are not found, no entities are returned for these IDs.
     *
     * Note that the order of elements in the result is not guaranteed.
     *
     * @param ids must not be null nor contain any null values.
     * @return guaranteed to be not null. The size can be equal or less than the number of given
     * ids.
     * @throws IllegalArgumentException in case the given [ids][Iterable] or one of its items is null.
     */
    fun allWithIds(ids: Iterable<TId>): Iterable<TEntity>

    /**
     * Deletes the entity with the given id.
     *
     * @param id must not be null.
     * @throws IllegalArgumentException in case the given id is null.
     * @return the number of entities that were deleted.
     */
    fun deleteWithId(id: TId): Long
}
