package org.hsmith.hibernate_decorator.data.entity.impl

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.PrePersist
import org.hsmith.hibernate_decorator.data.entity.EntityWithCreateTime

@MappedSuperclass
abstract class EntityWithCreateTimeBase : EntityBase(), EntityWithCreateTime {

    @Column(name = "creation_time")
    override var creationTime: LocalDateTime? = null

    @PrePersist
    protected fun prePersist() {
        creationTime = LocalDateTime.now()
    }
}
