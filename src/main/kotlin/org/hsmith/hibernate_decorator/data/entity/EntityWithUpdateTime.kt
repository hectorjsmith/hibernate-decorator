package org.hsmith.hibernate_decorator.data.entity

import java.time.LocalDateTime
/**
 * Database entity that defines an update time.
 * Implementations of this interface should reset the update time every time a new change is persisted.
 *
 * For storing the entity creation time see [EntityWithCreateTime].
 */
interface EntityWithUpdateTime : Entity {
    /**
     * The entity update time. This stores the time at which the entity was last modified.
     */
    var updateTime: LocalDateTime?
}
