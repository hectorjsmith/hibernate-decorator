package org.hsmith.hibernate_decorator.data.entity

/**
 * Database entity with an ID of a generic type.
 * This interface allows defining an ID of any type, but some types may not be allowed by the underlying database.
 */
interface EntityWithId<TIdType : Any> : Entity {
    /**
     * Entity ID.
     */
    var id: TIdType
}
