package org.hsmith.hibernate_decorator.data.entity.impl

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.MappedSuperclass
import javax.persistence.PrePersist
import javax.persistence.PreUpdate
import org.hsmith.hibernate_decorator.data.entity.EntityWithUpdateTime

@MappedSuperclass
abstract class EntityWithUpdateTimeBase : EntityBase(), EntityWithUpdateTime {

    @Column(name = "update_time")
    override var updateTime: LocalDateTime? = null

    @PreUpdate
    @PrePersist
    protected fun preUpdate() {
        updateTime = LocalDateTime.now()
    }
}
