package org.hsmith.hibernate_decorator.data.entity.impl

import javax.persistence.*
import org.hsmith.hibernate_decorator.data.entity.EntityWithId

@MappedSuperclass
abstract class EntityWithLongIdBase : EntityBase(), EntityWithId<Long> {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    override var id: Long = 0
}
