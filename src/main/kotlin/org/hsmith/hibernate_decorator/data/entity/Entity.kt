package org.hsmith.hibernate_decorator.data.entity

/**
 * Base interface for a database entity.
 * All other database entities extend this interface.
 */
interface Entity
