package org.hsmith.hibernate_decorator.data.entity

import java.time.LocalDateTime

/**
 * Database entity that defines a creation time.
 * Implementations of this interface should only set the creation time when the entity is first persisted.
 *
 * For storing the entity modification time see [EntityWithUpdateTime].
 */
interface EntityWithCreateTime : Entity {
    /**
     * Entity creation time.
     */
    var creationTime: LocalDateTime?
}
