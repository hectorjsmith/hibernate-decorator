package org.hsmith.hibernate_decorator.query.jpa

/**
 * Base interface for all underlying JPA queries.
 *
 * Instances of this interface run queries against the actual database.
 * Other interfaces leverage these queries to build more complex queries.
 */
interface JpaQuery
