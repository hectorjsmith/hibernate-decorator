package org.hsmith.hibernate_decorator.query.jpa.delete

import org.hsmith.hibernate_decorator.query.jpa.base.QueryRunnerBase
import org.hsmith.hibernate_decorator.query.where.WhereClause
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class DeleteWhereQueryImpl<TEntity : Any>(
    private val entityManagerDecorator: EntityManagerDecorator,
    private val handledType: Class<TEntity>
) : QueryRunnerBase(entityManagerDecorator), DeleteWhereQueryDecorator<TEntity> {

    private val jpqlObjectName = "o"

    override fun deleteAllWhere(clause: WhereClause): Long {
        clause.setObjectName(jpqlObjectName)
        val immutableClause = clause.toImmutableWhereClause()

        val queryString = "DELETE FROM ${handledType.simpleName} $jpqlObjectName WHERE ${immutableClause.jpqlString}"
        val rawQuery = entityManagerDecorator.rawEntityManager.createQuery(queryString)

        for (pair in immutableClause.propertyMap) {
            rawQuery.setParameter(pair.key, pair.value)
        }

        return runWithTransaction {
            rawQuery.executeUpdate()
        }.toLong()
    }
}
