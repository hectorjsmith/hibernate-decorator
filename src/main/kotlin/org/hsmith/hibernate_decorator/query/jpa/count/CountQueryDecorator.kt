package org.hsmith.hibernate_decorator.query.jpa.count

import org.hsmith.hibernate_decorator.query.jpa.JpaQuery
import org.hsmith.hibernate_decorator.query.where.WhereClause

/**
 * Query to count the number of entities that match a given condition.
 */
interface CountQueryDecorator<TEntity : Any> : JpaQuery {
    /**
     * Count all entities.
     */
    fun count(): Long

    /**
     * Count all entities where the provided [WhereClause] returns true.
     */
    fun count(clause: WhereClause): Long
}
