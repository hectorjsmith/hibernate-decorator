package org.hsmith.hibernate_decorator.query.jpa.delete

import org.hsmith.hibernate_decorator.query.jpa.base.QueryRunnerBase
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class DeleteQueryImpl<TEntity : Any>(
    private val entityManagerDecorator: EntityManagerDecorator
) : QueryRunnerBase(entityManagerDecorator), DeleteQueryDecorator<TEntity> {

    override fun <TEntityToDelete : TEntity> delete(entity: TEntityToDelete) {
        runWithTransaction {
            remove(entity)
        }
    }

    override fun <TEntityToDelete : TEntity> deleteAll(entities: Iterable<TEntityToDelete>) {
        runWithTransaction {
            for (entity in entities) {
                remove(entity)
            }
        }
    }

    private fun <TEntityToDelete : TEntity> remove(entity: TEntityToDelete) {
        val rawEntityManager = entityManagerDecorator.rawEntityManager
        rawEntityManager.remove(
                if (rawEntityManager.contains(entity))
                    entity
                else
                    rawEntityManager.merge(entity)
        )
    }
}
