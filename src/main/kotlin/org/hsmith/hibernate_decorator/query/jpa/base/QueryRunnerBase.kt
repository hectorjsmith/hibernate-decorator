package org.hsmith.hibernate_decorator.query.jpa.base

import org.hsmith.hibernate_decorator.session.EntityManagerDecorator
import org.slf4j.Logger
import org.slf4j.LoggerFactory

abstract class QueryRunnerBase(
    private val entityManagerDecorator: EntityManagerDecorator
) {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    protected fun <TReturnType : Any> runWithTransaction(
        action: (() -> TReturnType)
    ): TReturnType {
        prepareTransaction()
        return action()
    }

    private fun prepareTransaction() {
        if (!entityManagerDecorator.isTransactionActive) {
            logger.debug("Starting new transaction")
            entityManagerDecorator.startTransaction()
        }
    }
}
