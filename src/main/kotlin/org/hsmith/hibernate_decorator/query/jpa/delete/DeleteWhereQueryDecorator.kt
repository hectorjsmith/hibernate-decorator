package org.hsmith.hibernate_decorator.query.jpa.delete

import org.hsmith.hibernate_decorator.query.jpa.JpaQuery
import org.hsmith.hibernate_decorator.query.where.WhereClause

/**
 * Query to delete all entities in the database that match a given condition.
 */
interface DeleteWhereQueryDecorator<TEntity : Any> : JpaQuery {
    /**
     * Delete all entities where the provided [WhereClause] returns true.
     * Returns the number of entities deleted.
     */
    fun deleteAllWhere(clause: WhereClause): Long
}
