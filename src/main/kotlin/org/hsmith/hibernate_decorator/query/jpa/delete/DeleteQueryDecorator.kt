package org.hsmith.hibernate_decorator.query.jpa.delete

import org.hsmith.hibernate_decorator.query.jpa.JpaQuery

/**
 * Query to delete provided entities.
 */
interface DeleteQueryDecorator<TEntity : Any> : JpaQuery {
    /**
     * Delete the provided entity from the database.
     */
    fun <TEntityToDelete : TEntity> delete(entity: TEntityToDelete)

    /**
     * Delete all of the provided entities from the database.
     */
    fun <TEntityToDelete : TEntity> deleteAll(entities: Iterable<TEntityToDelete>)
}
