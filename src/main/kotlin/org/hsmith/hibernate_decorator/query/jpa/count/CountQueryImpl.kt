package org.hsmith.hibernate_decorator.query.jpa.count

import org.hsmith.hibernate_decorator.query.jpa.base.QueryRunnerBase
import org.hsmith.hibernate_decorator.query.where.WhereClause
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class CountQueryImpl<TEntity : Any>(
    private val entityManagerDecorator: EntityManagerDecorator,
    private val handledType: Class<TEntity>
) : QueryRunnerBase(entityManagerDecorator), CountQueryDecorator<TEntity> {

    private val jpqlObjectName: String = "o"

    override fun count(): Long {
        val queryString = "SELECT count(1) FROM ${handledType.simpleName} $jpqlObjectName"
        val rawQuery = entityManagerDecorator.rawEntityManager.createQuery(queryString, java.lang.Long::class.java)
        return runWithTransaction {
            rawQuery.singleResult as Long
        }
    }

    override fun count(clause: WhereClause): Long {
        clause.setObjectName(jpqlObjectName)
        val immutableClause = clause.toImmutableWhereClause()

        val queryString = "SELECT count(1) FROM ${handledType.simpleName} $jpqlObjectName WHERE ${immutableClause.jpqlString}"
        val rawQuery = entityManagerDecorator.rawEntityManager.createQuery(queryString, java.lang.Long::class.java)

        for (pair in immutableClause.propertyMap) {
            rawQuery.setParameter(pair.key, pair.value)
        }

        return runWithTransaction {
            rawQuery.singleResult as Long
        }
    }
}
