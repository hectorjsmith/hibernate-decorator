package org.hsmith.hibernate_decorator.query.jpa.save

import org.hsmith.hibernate_decorator.query.jpa.base.QueryRunnerBase
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class SaveOrUpdateQueryImpl<TReturnType : Any>(
    private val entityManagerDecorator: EntityManagerDecorator
) : QueryRunnerBase(entityManagerDecorator), SaveOrUpdateQueryDecorator<TReturnType> {

    override fun <TEntityToSave : TReturnType> save(entity: TEntityToSave): TEntityToSave {
        return runWithTransaction {
            entityManagerDecorator.rawEntityManager.merge(entity)
        }
    }

    override fun <TEntityToSave : TReturnType> saveAll(entities: Iterable<TEntityToSave>): Iterable<TEntityToSave> {
        val returnList: MutableCollection<TEntityToSave> = mutableListOf()
        runWithTransaction {
            for (entity in entities) {
                returnList.add(entityManagerDecorator.rawEntityManager.merge(entity))
            }
        }
        return returnList
    }
}
