package org.hsmith.hibernate_decorator.query.jpa.select

import java.util.*
import javax.persistence.TypedQuery
import org.hsmith.hibernate_decorator.query.jpa.base.QueryRunnerBase
import org.hsmith.hibernate_decorator.query.where.WhereClause
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class SelectQueryImpl<TEntity : Any>(
    private val entityManagerDecorator: EntityManagerDecorator,
    private val handledType: Class<TEntity>
) : QueryRunnerBase(entityManagerDecorator), SelectQueryDecorator<TEntity> {

    private val jpqlObjectName: String = "o"

    override fun selectAll(limit: Optional<Int>): Iterable<TEntity> {
        val queryString = "FROM ${handledType.simpleName}"
        val rawQuery = entityManagerDecorator.rawEntityManager.createQuery(queryString, handledType)
        limit.ifPresent { rawQuery.maxResults = it }
        return runWithTransaction {
            rawQuery.resultList
        }
    }

    override fun selectAllWhere(clause: WhereClause, limit: Optional<Int>): Iterable<TEntity> {
        val rawQuery = setupRawQuery(clause)
        limit.ifPresent { rawQuery.maxResults = it }
        return runWithTransaction {
            rawQuery.resultList
        }
    }

    override fun selectUnique(): TEntity {
        val queryString = "FROM ${handledType.simpleName}"
        val rawQuery = entityManagerDecorator.rawEntityManager.createQuery(queryString, handledType)
        return runWithTransaction {
            rawQuery.singleResult
        }
    }

    override fun selectUniqueWhere(clause: WhereClause): TEntity {
        val rawQuery = setupRawQuery(clause)
        return runWithTransaction {
            rawQuery.singleResult
        }
    }

    private fun setupRawQuery(clause: WhereClause): TypedQuery<TEntity> {
        clause.setObjectName(jpqlObjectName)
        val immutableClause = clause.toImmutableWhereClause()

        val queryString = "FROM ${handledType.simpleName} $jpqlObjectName WHERE ${immutableClause.jpqlString}"
        val rawQuery = entityManagerDecorator.rawEntityManager.createQuery(queryString, handledType)

        for (pair in immutableClause.propertyMap) {
            rawQuery.setParameter(pair.key, pair.value)
        }
        return rawQuery
    }
}
