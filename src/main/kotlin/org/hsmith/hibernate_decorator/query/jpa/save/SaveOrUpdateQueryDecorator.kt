package org.hsmith.hibernate_decorator.query.jpa.save

import org.hsmith.hibernate_decorator.query.jpa.JpaQuery

/**
 * Query to save entities to the database.
 */
interface SaveOrUpdateQueryDecorator<TEntity : Any> : JpaQuery {
    /**
     * Save the provided entity to the database.
     * If the entity already exists in the database, it will be updated.
     *
     * Returns the saved entity.
     * Note that the saved entity may have updated fields (for example, the entity ID).
     */
    fun <TEntityToSave : TEntity> save(entity: TEntityToSave): TEntityToSave

    /**
     * Save all of the provided entities to the database.
     * Any entities that already exist in the database will be updated.
     *
     * Returns an [Iterable] of the saved entities.
     * Note that the saved entities may have updated fields (for example, the entity ID).
     */
    fun <TEntityToSave : TEntity> saveAll(entities: Iterable<TEntityToSave>): Iterable<TEntityToSave>
}
