package org.hsmith.hibernate_decorator.query.jpa.select

import java.util.*
import org.hsmith.hibernate_decorator.query.jpa.JpaQuery
import org.hsmith.hibernate_decorator.query.where.WhereClause

/**
 * Query to select all entities that match a given condition.
 */
interface SelectQueryDecorator<TEntity> : JpaQuery {
    /**
     * Find and return all entities.
     */
    fun selectAll(limit: Optional<Int>): Iterable<TEntity>

    /**
     * Find and return all entities where the provided [WhereClause] returns true.
     */
    fun selectAllWhere(clause: WhereClause, limit: Optional<Int>): Iterable<TEntity>

    fun selectUnique(): TEntity

    /**
     * Find and return the single unique entity where the provided [WhereClause] returns true.
     * An exception is thrown if more than one entity is found
     */
    fun selectUniqueWhere(clause: WhereClause): TEntity
}
