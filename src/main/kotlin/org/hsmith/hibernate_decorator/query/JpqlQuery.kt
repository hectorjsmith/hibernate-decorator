package org.hsmith.hibernate_decorator.query

/**
 * Raw JPQL query handler.
 * This type of query handler supports retrieving results and executing updates.
 * The type of [TEntity] must match the type of entity being retrieved.
 */
interface JpqlQuery<TEntity : Any> : UpdateOnlyJpqlQuery {
    /**
     * Find and retrieve a single entity based on the provided JPQL query and parameter map.
     * If multiple matches are found, a [javax.persistence.NonUniqueResultException] is thrown.
     * If no matches are found, a [javax.persistence.NoResultException] is thrown.
     *
     * See [javax.persistence.TypedQuery.getSingleResult] for more details.
     */
    fun singleResult(): TEntity

    /**
     * Find and retrieve all entities that match the provided JPQL query and parameter map.
     *
     * See [javax.persistence.TypedQuery.getResultList] for more details.
     */
    fun resultList(): List<TEntity>

    /**
     * Find and retrieve all entities that match the provided JPQL query and parameter map.
     * Returned results are limited to the number provided.
     *
     * See [javax.persistence.TypedQuery.getResultList] for more details.
     */
    fun resultList(limit: Int): List<TEntity>
}
