package org.hsmith.hibernate_decorator.query.impl

import org.hsmith.hibernate_decorator.query.UpdateOnlyJpqlQuery
import org.hsmith.hibernate_decorator.query.jpa.base.QueryRunnerBase
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class UpdateOnlyJpqlQueryImpl(
    private val entityManagerDecorator: EntityManagerDecorator,
    private val queryString: String,
    private val parameterMap: Map<String, Any>
) : QueryRunnerBase(entityManagerDecorator), UpdateOnlyJpqlQuery {

    override fun executeUpdate(): Int {
        val rawQuery = entityManagerDecorator.rawEntityManager.createQuery(queryString)

        for (pair in parameterMap) {
            rawQuery.setParameter(pair.key, pair.value)
        }

        return runWithTransaction {
            rawQuery.executeUpdate()
        }
    }
}
