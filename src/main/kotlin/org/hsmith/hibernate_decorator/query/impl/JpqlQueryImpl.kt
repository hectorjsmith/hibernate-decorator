package org.hsmith.hibernate_decorator.query.impl

import java.util.*
import javax.persistence.TypedQuery
import org.hsmith.hibernate_decorator.query.JpqlQuery
import org.hsmith.hibernate_decorator.query.UpdateOnlyJpqlQuery
import org.hsmith.hibernate_decorator.query.jpa.base.QueryRunnerBase
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class JpqlQueryImpl<TEntity : Any>(
    private val updateOnlyJpqlQuery: UpdateOnlyJpqlQuery,
    private val handledClass: Class<TEntity>,
    private val entityManagerDecorator: EntityManagerDecorator,
    private val queryString: String,
    private val parameterMap: Map<String, Any>
) : JpqlQuery<TEntity>, UpdateOnlyJpqlQuery by updateOnlyJpqlQuery, QueryRunnerBase(entityManagerDecorator) {

    override fun singleResult(): TEntity {
        val rawQuery = buildQuery()
        return runWithTransaction {
            rawQuery.singleResult
        }
    }

    override fun resultList(): List<TEntity> {
        return resultList(Optional.empty())
    }

    override fun resultList(limit: Int): List<TEntity> {
        return resultList(Optional.of(limit))
    }

    private fun resultList(limit: Optional<Int>): List<TEntity> {
        val rawQuery = buildQuery()
        limit.ifPresent { rawQuery.maxResults = it }
        return runWithTransaction {
            rawQuery.resultList
        }
    }

    private fun buildQuery(): TypedQuery<TEntity> {
        val rawQuery = entityManagerDecorator.rawEntityManager.createQuery(queryString, handledClass)

        for (pair in parameterMap) {
            rawQuery.setParameter(pair.key, pair.value)
        }
        return rawQuery
    }
}
