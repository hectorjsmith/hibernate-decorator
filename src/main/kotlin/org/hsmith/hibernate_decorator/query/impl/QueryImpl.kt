package org.hsmith.hibernate_decorator.query.impl

import java.util.*
import javax.persistence.NoResultException
import org.hsmith.hibernate_decorator.query.Query
import org.hsmith.hibernate_decorator.query.jpa.count.CountQueryImpl
import org.hsmith.hibernate_decorator.query.jpa.delete.DeleteQueryImpl
import org.hsmith.hibernate_decorator.query.jpa.delete.DeleteWhereQueryImpl
import org.hsmith.hibernate_decorator.query.jpa.select.SelectQueryImpl
import org.hsmith.hibernate_decorator.query.where.ImmutableWhereClause
import org.hsmith.hibernate_decorator.query.where.WhereClause
import org.hsmith.hibernate_decorator.query.where.factory.WhereClauseFactory
import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator
import org.hsmith.hibernate_decorator.query.where.operator.ClauseOperator
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

class QueryImpl<TEntity : Any>(
    private val entityManagerDecorator: EntityManagerDecorator,
    private val handledType: Class<TEntity>,
    override val whereClauseFactory: WhereClauseFactory
) : Query<TEntity> {
    private var rawWhereClause: Optional<WhereClause> = Optional.empty()
    private var queryLimit: Optional<Int> = Optional.empty()

    override val whereClause: Optional<ImmutableWhereClause>
        get() = rawWhereClause.map { rawWhereClause.orElseGet(null)?.toImmutableWhereClause() }

    override fun where(fieldName: String, value: Any): Query<TEntity> {
        return where(whereClauseFactory.newWhereClause(fieldName, BinaryExpressionOperator.EQUAL, value))
    }

    override fun where(whereClause: WhereClause): Query<TEntity> {
        if (this.rawWhereClause.isEmpty) {
            this.rawWhereClause = Optional.of(whereClause)
        } else {
            val composedClause = whereClauseFactory.newWhereClause(this.rawWhereClause.get(), ClauseOperator.AND, whereClause)
            this.rawWhereClause = Optional.of(composedClause)
        }
        return this
    }

    override fun limit(limit: Int): Query<TEntity> {
        queryLimit = Optional.of(limit)
        return this
    }

    override fun exists(): Boolean {
        return count() > 0
    }

    override fun count(): Long {
        val query = CountQueryImpl(entityManagerDecorator, handledType)
        var count: Long = 0
        rawWhereClause.ifPresentOrElse({ count = query.count(it) }, { count = query.count() })
        return count
    }

    override fun single(): Optional<TEntity> {
        val query = SelectQueryImpl(entityManagerDecorator, handledType)
        var result: TEntity? = null
        try {
            rawWhereClause.ifPresentOrElse({ result = query.selectUniqueWhere(it) }, { result = query.selectUnique() })
        } catch (ex: NoResultException) {
            // Swallow exception and return an empty Optional if none found
        }
        return Optional.ofNullable(result)
    }

    override fun all(): Iterable<TEntity> {
        val query = SelectQueryImpl(entityManagerDecorator, handledType)
        var result: Iterable<TEntity> = listOf()
        rawWhereClause.ifPresentOrElse(
                { result = query.selectAllWhere(it, queryLimit) },
                { result = query.selectAll(queryLimit) })
        return result
    }

    override fun delete(): Long {
        return if (rawWhereClause.isPresent) {
            val query = DeleteWhereQueryImpl(entityManagerDecorator, handledType)
            query.deleteAllWhere(rawWhereClause.get())
        } else {
            val query = DeleteQueryImpl<TEntity>(entityManagerDecorator)
            val entitiesToDelete = all()
            query.deleteAll(entitiesToDelete)
            entitiesToDelete.count().toLong()
        }
    }
}
