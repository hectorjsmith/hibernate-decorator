package org.hsmith.hibernate_decorator.query

/**
 * Raw JPQL query handler.
 * This type of query handler only supports running updates.
 */
interface UpdateOnlyJpqlQuery {
    /**
     * Use the provided JPQL string and parameters to execute an update.
     * Returns the number of entries modified.
     *
     * See [javax.persistence.TypedQuery.executeUpdate] for more details.
     */
    fun executeUpdate(): Int
}
