package org.hsmith.hibernate_decorator.query

import java.util.*
import org.hsmith.hibernate_decorator.query.where.ImmutableWhereClause
import org.hsmith.hibernate_decorator.query.where.WhereClause
import org.hsmith.hibernate_decorator.query.where.factory.WhereClauseFactory

/**
 * Query object to allow building complex queries using a simple API.
 */
interface Query<TEntity : Any> {
    /**
     * Instance of [WhereClauseFactory] to allow creating complex [WhereClause] instances.
     *
     * See [where] to add where clauses to this query.
     */
    val whereClauseFactory: WhereClauseFactory

    /**
     * Get a read-only view of the where clause condition this query will use.
     *
     * See [where] for adding conditions to the where clause.
     */
    val whereClause: Optional<ImmutableWhereClause>

    /**
     * Add a new condition to this query.
     * The added condition checks that the provided fieldName matches the given value.
     * If multiple conditions are added to the same query they are combined using an AND operator.
     */
    fun where(fieldName: String, value: Any): Query<TEntity>

    /**
     * Add a new condition to this query using a [WhereClause].
     * If multiple conditions are added to the same query they are combined using an AND operator.
     */
    fun where(whereClause: WhereClause): Query<TEntity>

    /**
     * Limit the number of returned entities by this query.
     *
     * This limit only applies when combined with the [all] method.
     */
    fun limit(limit: Int): Query<TEntity>

    /**
     * Return true if any result is found that matches this query's conditions.
     */
    fun exists(): Boolean

    /**
     * Return the number of entities that match this query's conditions.
     */
    fun count(): Long

    /**
     * Return a single entity that matches this query's conditions.
     * If no matches found, an empty optional object is returned.
     * If multiple matches are found, an exception is thrown.
     */
    fun single(): Optional<TEntity>

    /**
     * Return all entities that match this query's conditions.
     */
    fun all(): Iterable<TEntity>

    /**
     * Delete all entities that match this query's conditions.
     * Returns the number of entities deleted.
     */
    fun delete(): Long
}
