package org.hsmith.hibernate_decorator.query.where

import org.hsmith.hibernate_decorator.data.repository.Repository

/**
 * Clause used when finding entities in the database.
 *
 * This interface is used as a way to strictly define the `where` section of an SQL query.
 * Implementations of this interface can support conditions between one, two, or more values.
 *
 * See [Repository] for uses.
 */
interface WhereClause {
    /**
     * Where clauses in JPQL queries define fields as `object.field` strings.
     * This method allows replacing `object` string with a custom name.
     * This is useful when defining composed clauses that reference different objects.
     */
    fun setObjectName(objectName: String)

    /**
     * Where clauses in JPQL queries use string placeholders for values.
     * This method allows replacing that placeholder string with a custom name.
     * This is useful when defining composed clauses that take different values.
     */
    fun setParameterName(parameterName: String)

    /**
     * Convert this instance into an [ImmutableWhereClause].
     */
    fun toImmutableWhereClause(): ImmutableWhereClause
}
