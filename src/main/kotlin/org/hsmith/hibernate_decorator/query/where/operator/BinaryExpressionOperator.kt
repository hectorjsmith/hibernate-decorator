package org.hsmith.hibernate_decorator.query.where.operator

/**
 * List af all supported binary operators to be used when creating [org.hsmith.hibernate_decorator.query.where.WhereClause] instances.
 */
enum class BinaryExpressionOperator(override val jpql: String) : ExpressionOperator {
    EQUAL("="),
    NOT_EQUAL("<>"),
    GREATER_THAN(">"),
    GREATER_THAN_OR_EQUAL(">="),
    LESS_THAN("<"),
    LESS_THAN_OR_EQUAL("<="),
    LIKE("LIKE"),
    NOT_LIKE("NOT LIKE"),
    IN("IN"),
    NOT_IN("NOT IN"),
    MEMBER_OF("MEMBER OF"),
    NOT_MEMBER_OF("NOT MEMBER OF");
}
