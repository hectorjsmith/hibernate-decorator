package org.hsmith.hibernate_decorator.query.where

import org.hsmith.hibernate_decorator.query.where.operator.ClauseOperator

class CompositeWhereClause(
    private val clause1: WhereClause,
    private val operator: ClauseOperator,
    private val clause2: WhereClause
) : WhereClause {

    init {
        setParameterName("param")
    }

    override fun setObjectName(objectName: String) {
        clause1.setObjectName(objectName)
        clause2.setObjectName(objectName)
    }

    override fun setParameterName(parameterName: String) {
        clause1.setParameterName(parameterName + "1")
        clause2.setParameterName(parameterName + "2")
    }

    override fun toImmutableWhereClause(): ImmutableWhereClause {
        val immutableClause1 = clause1.toImmutableWhereClause()
        val immutableClause2 = clause2.toImmutableWhereClause()

        return ImmutableWhereClauseImpl(
            buildJpqlString(immutableClause1, immutableClause2),
            buildPropertyMap(immutableClause1, immutableClause2)
        )
    }

    private fun buildJpqlString(clause1: ImmutableWhereClause, clause2: ImmutableWhereClause): String {
        return "(${clause1.jpqlString} ${operator.jpql} ${clause2.jpqlString})"
    }

    private fun buildPropertyMap(clause1: ImmutableWhereClause, clause2: ImmutableWhereClause): Map<String, Any> {
        return clause1.propertyMap.plus(clause2.propertyMap)
    }
}
