package org.hsmith.hibernate_decorator.query.where.operator

/**
 * List af all supported ternary operators to be used when creating [org.hsmith.hibernate_decorator.query.where.WhereClause] instances.
 */
enum class TernaryExpressionOperator(override val jpql: String) : ExpressionOperator {
    BETWEEN("BETWEEN"),
    NOT_BETWEEN("NOT BETWEEN"),
}
