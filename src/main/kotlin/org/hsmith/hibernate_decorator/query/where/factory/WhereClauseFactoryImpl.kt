package org.hsmith.hibernate_decorator.query.where.factory

import org.hsmith.hibernate_decorator.query.where.*
import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator
import org.hsmith.hibernate_decorator.query.where.operator.ClauseOperator
import org.hsmith.hibernate_decorator.query.where.operator.TernaryExpressionOperator
import org.hsmith.hibernate_decorator.query.where.operator.UnaryExpressionOperator

class WhereClauseFactoryImpl : WhereClauseFactory {
    override fun newWhereClause(operand1: String, operator: UnaryExpressionOperator): WhereClause {
        return UnaryWhereClause(operand1, operator)
    }

    override fun newWhereClause(operand1: String, operator: BinaryExpressionOperator, operand2: Any): WhereClause {
        return BinaryWhereClause(operand1, operator, operand2)
    }

    override fun newWhereClause(operand1: String, operator: TernaryExpressionOperator, operand2: Any, operand3: Any): WhereClause {
        return TernaryWhereClause(operand1, operator, operand2, operand3)
    }

    override fun newWhereClause(operand1: WhereClause, operator: ClauseOperator, operand2: WhereClause): WhereClause {
        return CompositeWhereClause(operand1, operator, operand2)
    }
}
