package org.hsmith.hibernate_decorator.query.where.operator

/**
 * List af all supported operators to be used when composing two [org.hsmith.hibernate_decorator.query.where.WhereClause] instances.
 */
enum class ClauseOperator(val jpql: String) {
    AND("AND"),
    OR("OR");
}
