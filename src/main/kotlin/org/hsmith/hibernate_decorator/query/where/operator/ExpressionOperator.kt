package org.hsmith.hibernate_decorator.query.where.operator

/**
 * Interface that defines the fields an expression operator should include.
 * Expression operators define operations that can be performed between various clauses.
 */
interface ExpressionOperator {
    /**
     * Get the JPQL string for this operator.
     */
    val jpql: String
}
