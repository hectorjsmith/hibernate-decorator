package org.hsmith.hibernate_decorator.query.where

class ImmutableWhereClauseImpl(
    override val jpqlString: String,
    override val propertyMap: Map<String, Any>
) : ImmutableWhereClause
