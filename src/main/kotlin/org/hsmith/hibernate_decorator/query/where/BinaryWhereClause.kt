package org.hsmith.hibernate_decorator.query.where

import org.hsmith.hibernate_decorator.query.where.operator.BinaryExpressionOperator

class BinaryWhereClause(
    private val operand1: String,
    private val operator: BinaryExpressionOperator,
    private val operand2: Any
) : WhereClause {

    private var parameterName: String = "param"
    private var objectName: String = "o"

    override fun setObjectName(objectName: String) {
        this.objectName = objectName
    }

    override fun setParameterName(parameterName: String) {
        this.parameterName = parameterName
    }

    override fun toImmutableWhereClause(): ImmutableWhereClause {
        return ImmutableWhereClauseImpl(buildJpqlString(), buildPropertyMap())
    }

    private fun buildJpqlString(): String {
        return "$objectName.$operand1 ${operator.jpql} :$parameterName"
    }

    private fun buildPropertyMap(): Map<String, Any> {
        return mapOf(Pair(parameterName, operand2))
    }
}
