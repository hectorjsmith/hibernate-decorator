package org.hsmith.hibernate_decorator.query.where.operator

/**
 * List af all supported unary operators to be used when creating [org.hsmith.hibernate_decorator.query.where.WhereClause] instances.
 */
enum class UnaryExpressionOperator(override val jpql: String) : ExpressionOperator {
    IS_NULL("IS NULL"),
    IS_NOT_NULL("IS NOT NULL"),
    IS_EMPTY("IS EMPTY"),
    IS_NOT_EMPTY("IS NOT EMPTY");
}
