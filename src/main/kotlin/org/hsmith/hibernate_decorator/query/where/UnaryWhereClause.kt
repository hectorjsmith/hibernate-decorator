package org.hsmith.hibernate_decorator.query.where

import org.hsmith.hibernate_decorator.query.where.operator.UnaryExpressionOperator

class UnaryWhereClause(
    private val operand1: String,
    private val operator: UnaryExpressionOperator
) : WhereClause {

    private var objectName: String = "o"

    override fun setObjectName(objectName: String) {
        this.objectName = objectName
    }

    override fun setParameterName(parameterName: String) {
    }

    override fun toImmutableWhereClause(): ImmutableWhereClause {
        return ImmutableWhereClauseImpl(buildJpqlString(), mapOf())
    }

    private fun buildJpqlString(): String {
        return "$objectName.$operand1 ${operator.jpql}"
    }
}
