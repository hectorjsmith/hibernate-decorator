package org.hsmith.hibernate_decorator.query.where

/**
 * An immutable version of the [WhereClause].
 */
interface ImmutableWhereClause {
    /**
     * Get the JPQL representation of this where clause.
     *
     * Note that this string will include placeholders for values.
     * See [propertyMap] to get a map of all placeholders to be replaced.
     */
    val jpqlString: String

    /**
     * Get a map of all replacements to perform in this query.
     * The query defined by the [jpqlString] includes placeholders strings that need to be replace with values
     * before the query can be performed.
     * This maps all placeholder strings to their corresponding value.
     */
    val propertyMap: Map<String, Any>
}
