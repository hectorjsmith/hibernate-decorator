package org.hsmith.hibernate_decorator.query.where

import org.hsmith.hibernate_decorator.query.where.operator.TernaryExpressionOperator

class TernaryWhereClause(
    private val operand1: String,
    private val operator: TernaryExpressionOperator,
    private val operand2: Any,
    private val operand3: Any
) : WhereClause {

    private var baseParameterName: String = "param"
    private var objectName: String = "o"
    private val parameter1Name get() = baseParameterName + "_a"
    private val parameter2Name get() = baseParameterName + "_b"

    override fun setObjectName(objectName: String) {
        this.objectName = objectName
    }

    override fun setParameterName(parameterName: String) {
        baseParameterName = parameterName
    }

    override fun toImmutableWhereClause(): ImmutableWhereClause {
        return ImmutableWhereClauseImpl(buildJpqlString(), buildPropertyMap())
    }

    private fun buildJpqlString(): String {
        return "$objectName.$operand1 ${operator.jpql} :$parameter1Name AND :$parameter2Name"
    }

    private fun buildPropertyMap(): Map<String, Any> {
        return mapOf(
                Pair(parameter1Name, operand2),
                Pair(parameter2Name, operand3)
        )
    }
}
