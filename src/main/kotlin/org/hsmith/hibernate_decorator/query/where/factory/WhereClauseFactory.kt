package org.hsmith.hibernate_decorator.query.where.factory

import org.hsmith.hibernate_decorator.query.where.WhereClause
import org.hsmith.hibernate_decorator.query.where.operator.*

/**
 * Builder class to simplify the creation of new [WhereClause] objects.
 *
 * See [org.hsmith.hibernate_decorator.api.HibernateDecoratorApi.newWhereClauseFactory].
 */
interface WhereClauseFactory {
    /**
     * Create a new unary [WhereClause].
     * This means a where clause that only takes one value. For example, [UnaryExpressionOperator.IS_EMPTY].
     *
     * See the [UnaryExpressionOperator] enum for available unary operators.
     */
    fun newWhereClause(operand1: String, operator: UnaryExpressionOperator): WhereClause

    /**
     * Create a new binary [WhereClause].
     * This means a where clause that takes two values and compares one to the other.
     * For example [BinaryExpressionOperator.EQUAL].
     *
     * See the [BinaryExpressionOperator] enum for available binary operators.
     */
    fun newWhereClause(operand1: String, operator: BinaryExpressionOperator, operand2: Any): WhereClause

    /**
     * Create a new ternary [WhereClause].
     * This means a where clause that compares three values.
     * For example [TernaryExpressionOperator.BETWEEN].
     *
     * See the [TernaryExpressionOperator] enum for available ternary operators.
     */
    fun newWhereClause(operand1: String, operator: TernaryExpressionOperator, operand2: Any, operand3: Any): WhereClause

    /**
     * Create a new composite [WhereClause].
     * This means a single where clause that contains two other where clauses.
     * This is used to support where clauses such as: `a = b AND b = c`.
     *
     * See the [ClauseOperator] enum for available operators between clauses.
     */
    fun newWhereClause(operand1: WhereClause, operator: ClauseOperator, operand2: WhereClause): WhereClause
}
