package org.hsmith.hibernate_decorator.api

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory

/**
 * Database API instance.
 *
 * This interface represents a database connection and is the starting point for all database access.
 * This instance wraps around a JPA [EntityManagerFactory]
 */
interface DbApi : AutoCloseable {
    /**
     * Raw [EntityManagerFactory] object that backs this database API instance.
     */
    val rawEntityManagerFactory: EntityManagerFactory

    /**
     * Indicates whether the underlying [EntityManagerFactory] is open.
     * Returns true until the factory has been closed.
     */
    val isOpen: Boolean

    /**
     * Get a new DbSession object. This is backed by a JPA [EntityManager] and is required for all database access
     * using this library.
     *
     * @return a new DbSession object backed by a new JPA [EntityManager].
     */
    fun newSession(): DbSession
}
