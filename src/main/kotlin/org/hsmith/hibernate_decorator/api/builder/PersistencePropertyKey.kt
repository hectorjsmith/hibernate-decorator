package org.hsmith.hibernate_decorator.api.builder

/**
 * Enum with commonly used database property override keys.
 * This is used when adding overrides in the [DbApiBuilder] class.
 */
enum class PersistencePropertyKey private constructor(val keyString: String) {
    DRIVER_CLASS("hibernate.connection.driver_class"),
    CONNECTION_URL("hibernate.connection.url"),
    USERNAME("hibernate.connection.username"),
    PASSWORD("hibernate.connection.password"),
    DIALECT("hibernate.dialect"),
    SHOW_SQL("hibernate.show_sql"),
    HBM2DDL_MODE("hibernate.hbm2ddl.auto")
}
