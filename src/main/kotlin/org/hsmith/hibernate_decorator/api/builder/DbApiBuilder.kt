package org.hsmith.hibernate_decorator.api.builder

import javax.persistence.EntityManagerFactory
import org.hsmith.hibernate_decorator.api.DbApi

/**
 * Builder object to create a new [DbApi] instance.
 */
interface DbApiBuilder {
    /**
     * Read-only list of all property overrides currently set.
     * These overrides are applied before the [EntityManagerFactory] instance is created.
     *
     * Note that this map does not include properties loaded from the `persistence.xml` file.
     * Only manually added overrides are included.
     */
    val propertyOverrides: Map<String, String>

    /**
     * Provide an existing EntityManagerFactory instance to back the DbApi instance.
     * When this option is used all other options are ignored and no new EntityManagerFactory is created.
     *
     * @return this object for method chaining.
     */
    fun withEntityManagerFactory(entityManagerFactory: EntityManagerFactory): DbApiBuilder

    /**
     * Set the name of the persistence unit to use. By default the name "default" is used.
     * The persistence unit name is case-sensitive and an exception will be thrown on build if the name does not exist.
     *
     * @return this object for method chaining.
     */
    fun withPersistenceUnitName(name: String): DbApiBuilder

    /**
     * Add an override for the database username property.
     * This value is used for database authentication.
     * The value provided will override any value found in the `persistence.xml` file.
     */
    fun withUsername(username: String): DbApiBuilder

    /**
     * Add an override for the database password property.
     * This value is used for database authentication.
     * The value provided will override any value found in the `persistence.xml` file.
     */
    fun withPassword(password: String): DbApiBuilder

    /**
     * Add an override for the database connection URL.
     * This value controls what database type to use and how to connect to it.
     * The value provided will override any value found in the `persistence.xml` file.
     */
    fun withConnectionUrl(url: String): DbApiBuilder

    /**
     * Override property that enables or disables printing SQL queries to the console.
     */
    fun withShowSql(showSql: Boolean): DbApiBuilder

    /**
     * Add a manual override for the property with the provided [key].
     */
    fun withPersistencePropertyOverride(key: String, value: String): DbApiBuilder

    /**
     * Add a manual override for the property with the provided [key].
     * The [PersistencePropertyKey] enum defines a set of commonly used override keys.
     */
    fun withPersistencePropertyOverride(key: PersistencePropertyKey, value: String): DbApiBuilder

    /**
     * Build and return new instance of the DbApi interface.
     *
     * If an [EntityManagerFactory] was provided, that instance will be used (see [withEntityManagerFactory]).
     * Otherwise, a new instance is created using the properties found in the `persistence.xml` file and all
     * manual overrides provided.
     */
    fun build(): DbApi
}
