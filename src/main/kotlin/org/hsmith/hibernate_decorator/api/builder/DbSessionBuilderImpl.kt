package org.hsmith.hibernate_decorator.api.builder

import java.util.*
import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import org.hsmith.hibernate_decorator.api.DbSession
import org.hsmith.hibernate_decorator.api.impl.DbSessionImpl
import org.hsmith.hibernate_decorator.session.impl.EntityManagerDecoratorImpl
import org.hsmith.hibernate_decorator.session.sessionid.impl.RandomSessionIdGenerator

class DbSessionBuilderImpl : DbSessionBuilder {
    private val rawEntityManager: EntityManager
    private var rawSessionId: Optional<String> = Optional.empty()
    private val sessionIdGenerator = RandomSessionIdGenerator()

    constructor(entityManager: EntityManager) {
        rawEntityManager = entityManager
    }

    constructor(rawEntityManagerFactory: EntityManagerFactory) {
        rawEntityManager = rawEntityManagerFactory.createEntityManager()
    }

    override fun withSessionId(sessionId: String): DbSessionBuilder {
        this.rawSessionId = Optional.of(sessionId)
        return this
    }

    override fun build(): DbSession {
        val sessionId = rawSessionId.orElseGet { sessionIdGenerator.generateNewSessionId() }
        val entityManagerDecorator = EntityManagerDecoratorImpl(sessionId, rawEntityManager)
        return DbSessionImpl(entityManagerDecorator)
    }
}
