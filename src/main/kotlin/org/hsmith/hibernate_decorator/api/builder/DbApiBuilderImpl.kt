package org.hsmith.hibernate_decorator.api.builder

import javax.persistence.EntityManagerFactory
import org.hsmith.hibernate_decorator.api.DbApi
import org.hsmith.hibernate_decorator.api.impl.DbApiImpl
import org.hsmith.hibernate_decorator.session.EntityManagerFactoryDecorator
import org.hsmith.hibernate_decorator.session.impl.EntityManagerFactoryDecoratorImpl

class DbApiBuilderImpl : DbApiBuilder {
    private val persistencePropertyOverrides: MutableMap<String, String> = HashMap()
    private var persistenceUnitName: String = "default"
    private var rawEntityManagerFactory: EntityManagerFactory? = null

    override val propertyOverrides: Map<String, String>
        get() = persistencePropertyOverrides

    override fun withEntityManagerFactory(entityManagerFactory: EntityManagerFactory): DbApiBuilder {
        rawEntityManagerFactory = entityManagerFactory
        return this
    }

    override fun withPersistenceUnitName(name: String): DbApiBuilder {
        persistenceUnitName = name
        return this
    }

    override fun withUsername(username: String): DbApiBuilder {
        return withPersistencePropertyOverride(PersistencePropertyKey.USERNAME, username)
    }

    override fun withPassword(password: String): DbApiBuilder {
        return withPersistencePropertyOverride(PersistencePropertyKey.PASSWORD, password)
    }

    override fun withConnectionUrl(url: String): DbApiBuilder {
        return withPersistencePropertyOverride(PersistencePropertyKey.CONNECTION_URL, url)
    }

    override fun withShowSql(showSql: Boolean): DbApiBuilder {
        return withPersistencePropertyOverride(PersistencePropertyKey.SHOW_SQL, showSql.toString())
    }

    override fun withPersistencePropertyOverride(key: String, value: String): DbApiBuilder {
        persistencePropertyOverrides[key] = value
        return this
    }

    override fun withPersistencePropertyOverride(key: PersistencePropertyKey, value: String): DbApiBuilder {
        return withPersistencePropertyOverride(key.keyString, value)
    }

    override fun build(): DbApi {
        return DbApiImpl(buildEntityManagerFactoryDecorator())
    }

    private fun buildEntityManagerFactoryDecorator(): EntityManagerFactoryDecorator {
        return if (rawEntityManagerFactory == null) {
            EntityManagerFactoryDecoratorImpl(persistenceUnitName, persistencePropertyOverrides)
        } else {
            EntityManagerFactoryDecoratorImpl(rawEntityManagerFactory!!)
        }
    }
}
