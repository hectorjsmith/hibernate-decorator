package org.hsmith.hibernate_decorator.api.builder

import org.hsmith.hibernate_decorator.api.DbSession

/**
 * Builder object responsible for creating new [DbSession] instances.
 */
interface DbSessionBuilder {
    /**
     * Set the session ID used by the [DbSession].
     * By default, a new random session ID is generated.
     */
    fun withSessionId(sessionId: String): DbSessionBuilder

    /**
     * Build the new [DbSession] instance based on the provided data.
     */
    fun build(): DbSession
}
