package org.hsmith.hibernate_decorator.api

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import org.hsmith.hibernate_decorator.api.builder.DbApiBuilder
import org.hsmith.hibernate_decorator.api.builder.DbApiBuilderImpl
import org.hsmith.hibernate_decorator.api.builder.DbSessionBuilder
import org.hsmith.hibernate_decorator.api.builder.DbSessionBuilderImpl
import org.hsmith.hibernate_decorator.query.where.WhereClause
import org.hsmith.hibernate_decorator.query.where.factory.WhereClauseFactory
import org.hsmith.hibernate_decorator.query.where.factory.WhereClauseFactoryImpl

/**
 * Main API class for this library.
 *
 * Includes utility methods to create other new builders and factories.
 */
object HibernateDecoratorApi {
    /**
     * Return a new [DbApiBuilder] instance to allow creating new [DbApi] instances.
     */
    fun newDbApiBuilder(): DbApiBuilder {
        return DbApiBuilderImpl()
    }

    /**
     * Build a new [DbSessionBuilder] instance based on the provided [EntityManagerFactory].
     */
    fun newDbSessionBuilder(entityManagerFactory: EntityManagerFactory): DbSessionBuilder {
        return DbSessionBuilderImpl(entityManagerFactory)
    }

    /**
     * Build a new [DbSessionBuilder] instance based on the provided [EntityManager].
     */
    fun newDbSessionBuilder(entityManager: EntityManager): DbSessionBuilder {
        return DbSessionBuilderImpl(entityManager)
    }

    /**
     * Return a new [WhereClauseFactory] instance to create new [WhereClause] instances.
     */
    fun newWhereClauseFactory(): WhereClauseFactory {
        return WhereClauseFactoryImpl()
    }
}
