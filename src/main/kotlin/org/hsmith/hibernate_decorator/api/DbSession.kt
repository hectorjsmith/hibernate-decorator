package org.hsmith.hibernate_decorator.api

import javax.persistence.EntityManager
import org.hsmith.hibernate_decorator.data.entity.Entity
import org.hsmith.hibernate_decorator.data.entity.EntityWithId
import org.hsmith.hibernate_decorator.data.repository.EntityRepository
import org.hsmith.hibernate_decorator.data.repository.EntityWithIdRepository
import org.hsmith.hibernate_decorator.data.repository.Repository
import org.hsmith.hibernate_decorator.query.JpqlQuery
import org.hsmith.hibernate_decorator.query.UpdateOnlyJpqlQuery
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

/**
 * Database session instance.
 *
 * This session object wraps around a JPA [EntityManager] and exposes methods to build entity repository objects.
 */
interface DbSession : AutoCloseable {
    /**
     * [EntityManagerDecorator] object that backs this session object.
     */
    val entityManagerDecorator: EntityManagerDecorator

    /**
     * Return true if this session is open and has an active transaction.
     */
    val isActive: Boolean

    /**
     * String that uniquely identifies this session.
     */
    val sessionId: String

    /**
     * Flush all pending changes to the underlying database.
     *
     * See [EntityManager.flush].
     */
    fun flush()

    /**
     * Build a new repository instance for [TEntity].
     */
    fun <TEntity : Any> newRepository(handledClass: Class<TEntity>): Repository<TEntity>

    /**
     * Build a new entity repository instance for [TEntity].
     */
    fun <TEntity : Entity> newRepository(handledClass: Class<TEntity>): EntityRepository<TEntity>

    /**
     * Build a new repository instance for an entity that inherits from the [EntityWithId] interface.
     */
    fun <TEntity : EntityWithId<TId>, TId : Any> newRepository(handledClass: Class<TEntity>): EntityWithIdRepository<TEntity, TId>

    /**
     * Build a new [UpdateOnlyJpqlQuery] object based on the given JPQL query string.
     */
    fun newUpdateOnlyJpqlQuery(queryString: String): UpdateOnlyJpqlQuery

    /**
     * Build a new [UpdateOnlyJpqlQuery] object based on the given JPQL query string and parameter map.
     * Query parameters can be defined in the query string using the `:name` format.
     * Include values for each parameter in the map using the parameter name as the key.
     */
    fun newUpdateOnlyJpqlQuery(queryString: String, parameterMap: Map<String, Any>): UpdateOnlyJpqlQuery

    /**
     * Build a new [JpqlQuery] object based on the given JPQL query string.
     * The returned query object will allow selecting entities of type [TEntity].
     */
    fun <TEntity : Any> newJpqlQuery(resultType: Class<TEntity>, queryString: String): JpqlQuery<TEntity>

    /**
     * Build a new [JpqlQuery] object based on the given JPQL query string and parameter map.
     * Query parameters can be defined in the query string using the `:name` format.
     * Include values for each parameter in the map using the parameter name as the key.
     * The returned query object will allow selecting entities of type [TEntity].
     */
    fun <TEntity : Any> newJpqlQuery(resultType: Class<TEntity>, queryString: String, parameterMap: Map<String, Any>): JpqlQuery<TEntity>
}
