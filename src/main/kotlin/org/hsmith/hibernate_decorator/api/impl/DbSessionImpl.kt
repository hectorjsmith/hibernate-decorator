package org.hsmith.hibernate_decorator.api.impl

import org.hsmith.hibernate_decorator.api.DbSession
import org.hsmith.hibernate_decorator.data.entity.Entity
import org.hsmith.hibernate_decorator.data.entity.EntityWithId
import org.hsmith.hibernate_decorator.data.repository.EntityRepository
import org.hsmith.hibernate_decorator.data.repository.EntityWithIdRepository
import org.hsmith.hibernate_decorator.data.repository.Repository
import org.hsmith.hibernate_decorator.data.repository.impl.EntityRepositoryImpl
import org.hsmith.hibernate_decorator.data.repository.impl.EntityWithIdRepositoryImpl
import org.hsmith.hibernate_decorator.data.repository.impl.RepositoryImpl
import org.hsmith.hibernate_decorator.query.JpqlQuery
import org.hsmith.hibernate_decorator.query.UpdateOnlyJpqlQuery
import org.hsmith.hibernate_decorator.query.impl.JpqlQueryImpl
import org.hsmith.hibernate_decorator.query.impl.UpdateOnlyJpqlQueryImpl
import org.hsmith.hibernate_decorator.query.where.factory.WhereClauseFactory
import org.hsmith.hibernate_decorator.query.where.factory.WhereClauseFactoryImpl
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

internal class DbSessionImpl(
    override val entityManagerDecorator: EntityManagerDecorator
) : DbSession {

    override val isActive: Boolean
        get() = (entityManagerDecorator.isSessionOpen && entityManagerDecorator.isTransactionActive)

    override val sessionId: String
        get() = entityManagerDecorator.sessionId

    override fun close() {
        entityManagerDecorator.close()
    }

    override fun flush() {
        entityManagerDecorator.rawEntityManager.flush()
    }

    override fun <TEntity : Any> newRepository(handledClass: Class<TEntity>): Repository<TEntity> {
        return RepositoryImpl(entityManagerDecorator, handledClass, buildNewWhereClauseFactory())
    }

    override fun <TEntity : Entity> newRepository(handledClass: Class<TEntity>): EntityRepository<TEntity> {
        return EntityRepositoryImpl(RepositoryImpl(entityManagerDecorator, handledClass, buildNewWhereClauseFactory()))
    }

    override fun <TEntity : EntityWithId<TId>, TId : Any> newRepository(handledClass: Class<TEntity>): EntityWithIdRepository<TEntity, TId> {
        return EntityWithIdRepositoryImpl(
                entityManagerDecorator,
                handledClass,
                buildNewWhereClauseFactory(),
                EntityRepositoryImpl(RepositoryImpl(entityManagerDecorator, handledClass, buildNewWhereClauseFactory()))
        )
    }

    override fun newUpdateOnlyJpqlQuery(queryString: String): UpdateOnlyJpqlQuery {
        return newUpdateOnlyJpqlQuery(queryString, mapOf())
    }

    override fun newUpdateOnlyJpqlQuery(queryString: String, parameterMap: Map<String, Any>): UpdateOnlyJpqlQuery {
        return UpdateOnlyJpqlQueryImpl(entityManagerDecorator, queryString, parameterMap)
    }

    override fun <TEntity : Any> newJpqlQuery(resultType: Class<TEntity>, queryString: String): JpqlQuery<TEntity> {
        return newJpqlQuery(resultType, queryString, mapOf())
    }

    override fun <TEntity : Any> newJpqlQuery(resultType: Class<TEntity>, queryString: String, parameterMap: Map<String, Any>): JpqlQuery<TEntity> {
        return JpqlQueryImpl(
                newUpdateOnlyJpqlQuery(queryString, parameterMap),
                resultType,
                entityManagerDecorator,
                queryString,
                parameterMap)
    }

    private fun buildNewWhereClauseFactory(): WhereClauseFactory {
        return WhereClauseFactoryImpl()
    }
}
