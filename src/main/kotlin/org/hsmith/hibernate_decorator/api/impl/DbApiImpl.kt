package org.hsmith.hibernate_decorator.api.impl

import javax.persistence.EntityManagerFactory
import org.hsmith.hibernate_decorator.api.DbApi
import org.hsmith.hibernate_decorator.api.DbSession
import org.hsmith.hibernate_decorator.api.builder.DbSessionBuilderImpl
import org.hsmith.hibernate_decorator.session.EntityManagerFactoryDecorator

internal class DbApiImpl(
    private val entityManagerFactoryDecorator: EntityManagerFactoryDecorator
) : DbApi {

    override val rawEntityManagerFactory: EntityManagerFactory
        get() = entityManagerFactoryDecorator.rawEntityManagerFactory

    override val isOpen: Boolean
        get() = rawEntityManagerFactory.isOpen

    override fun newSession(): DbSession {
        val builder = DbSessionBuilderImpl(rawEntityManagerFactory)
        return builder.build()
    }

    @Throws(Exception::class)
    override fun close() {
        if (isOpen) {
            rawEntityManagerFactory.close()
        }
    }
}
