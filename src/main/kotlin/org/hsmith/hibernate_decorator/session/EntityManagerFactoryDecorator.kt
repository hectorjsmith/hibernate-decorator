package org.hsmith.hibernate_decorator.session

import javax.persistence.EntityManagerFactory

/**
 * Wrapper around a [EntityManagerFactory] instance.
 *
 * The purpose of this interface is to simplify using an [EntityManagerFactory] instance and provide extra functionality.
 */
interface EntityManagerFactoryDecorator {
    /**
     * Build a new entity manager decorator object with the provided session ID.
     *
     * @param sessionId session ID to use when building the decorator. This should be unique.
     * @return a new decorator object wrapping a new entity manager.
     */
    fun newEntityManagerDecorator(sessionId: String): EntityManagerDecorator

    /**
     * Get the raw entity manager factory object wrapped by this object.
     *
     * @return the raw entity manager factory.
     */
    val rawEntityManagerFactory: EntityManagerFactory
}
