package org.hsmith.hibernate_decorator.session

import javax.persistence.EntityManager

/**
 * Wrapper around an [EntityManager] instance.
 *
 * The purpose of this interface is to simplify using an [EntityManager] instance and provide extra functionality.
 */
interface EntityManagerDecorator : AutoCloseable {
    /**
     * Get the database session ID.
     *
     * @return unique session ID string.
     */
    val sessionId: String

    /**
     * Check if the session is open for new database operations.
     *
     * @return true if the session is open, false otherwise.
     */
    val isSessionOpen: Boolean

    /**
     * Get the raw entity manager object that backs this decorator.
     * This should not be used unless strictly necessary.
     *
     * @return raw entity manager object.
     */
    val rawEntityManager: EntityManager

    /**
     * Check if there is any active transaction in this session.
     * The transaction is used to encapsulate multiple database accesses
     * in a single block that can be committed or reverted at once.
     *
     * @return true if there is an active transaction, false otherwise.
     */
    val isTransactionActive: Boolean

    /**
     * Start a new transaction.
     */
    fun startTransaction()

    /**
     * Commit the current transaction to the database.
     */
    fun commitTransaction()

    /**
     * Rollback all uncommitted changes in the current transaction.
     */
    fun rollBackTransaction()
}
