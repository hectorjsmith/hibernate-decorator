package org.hsmith.hibernate_decorator.session.sessionid

import org.hsmith.hibernate_decorator.session.EntityManagerDecorator

/**
 * Handle the creation of new session IDs for the [EntityManagerDecorator] class.
 */
interface SessionIdGenerator {
    /**
     * Generate a new session ID.
     */
    fun generateNewSessionId(): String
}
