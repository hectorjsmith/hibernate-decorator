package org.hsmith.hibernate_decorator.session.sessionid.impl

import java.util.*
import org.hsmith.hibernate_decorator.session.sessionid.SessionIdGenerator

class RandomSessionIdGenerator : SessionIdGenerator {
    @Synchronized
    override fun generateNewSessionId(): String {
        return UUID.randomUUID()!!.toString()
    }
}
