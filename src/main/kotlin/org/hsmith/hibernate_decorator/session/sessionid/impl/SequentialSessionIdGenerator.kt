package org.hsmith.hibernate_decorator.session.sessionid.impl

import org.hsmith.hibernate_decorator.session.sessionid.SessionIdGenerator

class SequentialSessionIdGenerator @JvmOverloads constructor(private val prefix: String = "") : SessionIdGenerator {
    private val outputFormat = "%s%d"
    private var sequence: Long = 0

    @Synchronized
    override fun generateNewSessionId(): String {
        sequence++
        return String.format(outputFormat, prefix, sequence)
    }
}
