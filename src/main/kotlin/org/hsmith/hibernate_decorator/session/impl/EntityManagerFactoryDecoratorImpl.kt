package org.hsmith.hibernate_decorator.session.impl

import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator
import org.hsmith.hibernate_decorator.session.EntityManagerFactoryDecorator
import org.slf4j.Logger
import org.slf4j.LoggerFactory

internal class EntityManagerFactoryDecoratorImpl : EntityManagerFactoryDecorator {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)
    override val rawEntityManagerFactory: EntityManagerFactory

    constructor(persistenceUnitName: String, persistencePropertyOverrides: Map<String, String>) {
        this.rawEntityManagerFactory = setupSessionFactory(persistenceUnitName, persistencePropertyOverrides)
    }

    constructor(entityManagerFactory: EntityManagerFactory) {
        rawEntityManagerFactory = entityManagerFactory
    }

    override fun newEntityManagerDecorator(sessionId: String): EntityManagerDecorator {
        return createNewDatabaseContext(sessionId)
    }

    private fun createNewDatabaseContext(sessionId: String): EntityManagerDecorator {
        val session = rawEntityManagerFactory.createEntityManager()
        logger.debug(String.format("[%s] New entity manager created", sessionId))
        return EntityManagerDecoratorImpl(sessionId, session)
    }

    private fun setupSessionFactory(persistenceUnitName: String, persistencePropertyOverrides: Map<String, String>): EntityManagerFactory {
        return Persistence.createEntityManagerFactory(persistenceUnitName, persistencePropertyOverrides)
    }
}
