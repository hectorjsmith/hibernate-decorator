package org.hsmith.hibernate_decorator.session.impl

import javax.persistence.EntityManager
import org.hsmith.hibernate_decorator.session.EntityManagerDecorator
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * Container class that contains information about a database session and transaction
 */
internal class EntityManagerDecoratorImpl(
    override val sessionId: String,
    override val rawEntityManager: EntityManager
) : EntityManagerDecorator {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override val isTransactionActive: Boolean
        get() = rawEntityManager.transaction.isActive

    override val isSessionOpen: Boolean
        get() = rawEntityManager.isOpen

    override fun startTransaction() {
        if (isTransactionActive) {
            logger.warn("Transaction already started, commit or rollback first")
            return
        }
        rawEntityManager.transaction.begin()
    }

    override fun commitTransaction() {
        if (!isTransactionActive) {
            logger.warn("Transaction not active, cannot commit")
            return
        }
        if (rawEntityManager.transaction.rollbackOnly) {
            logger.warn("Transaction is rollback only, cannot commit")
            return
        }
        rawEntityManager.transaction.commit()
    }

    override fun rollBackTransaction() {
        if (!isTransactionActive) {
            logger.warn("Transaction not active, cannot rollback")
            return
        }
        rawEntityManager.transaction.rollback()
    }

    override fun close() {
        if (!isSessionOpen) {
            return
        }
        logger.debug(String.format("[%s] Closing entity manager", sessionId))
        if (isTransactionActive) {
            if (rawEntityManager.transaction.rollbackOnly) {
                rollBackTransaction()
            } else {
                commitTransaction()
            }
        }
        rawEntityManager.close()
    }

    @Throws(Throwable::class)
    protected fun finalize() {
        if (isSessionOpen) {
            close()
            logger.warn(String.format("[%s] Session wasn't closed correctly", sessionId))
        }
    }
}
